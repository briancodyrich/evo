Strict

Import idle_prototype
	
Class MainMenuScreen Extends nScreen
	Field startBtn:MenuButton
	Field loadBtn:MenuButton
	Field exitBtn:MenuButton
	Field editorBtn:MenuButton
	Field logo:Image
	Method New()
		startBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 200 / 2, 160, 56)
		loadBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 320 / 2, 160, 56)
		exitBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 440 / 2, 160, 56)
		editorBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 560 / 2, 160, 56)
		startBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		loadBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		exitBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		editorBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		logo = LoadImage("logo.jpg",, Image.MidHandle)
		Game.white_font.Resize(0.5)
		
		Local p:Puzzle = New Puzzle()
		
		
	End
	Method Init:Int()
		Return 0
	End
	
	Method Train:Void(n:Network, times:int)
		
	End
	
	Method Write:Void(n:Network)
		Local out:Float[] = n.GetOutput([0.0, 0.0])
		Print("output " + out[0])
		out = n.GetOutput([1.0, 0.0])
		Print("output " + out[0])
		out = n.GetOutput([0.0, 1.0])
		Print("output " + out[0])
		out = n.GetOutput([1.0, 1.0])
		Print("output " + out[0])
	End
	
	
	Method Update:Int()

		If KeyHit(KEY_SPACE)
		
		End
		startBtn.Poll()
		If startBtn.hit
			Game.DoSwitch(Screens.ProtoGame)
		End
		'Game.white_font.Draw(
		loadBtn.Poll()
		exitBtn.Poll()
		If exitBtn.hit
			Game.Stop()
		End
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Stop()
		End
		
	
		Return 0
	End
	
	
	Method Render:Int()
		#rem
		Const DAM_S:Int = 0
		Const DAM_N:Int = 1
		Const DAM_E:Int = 2
		Const DAM_W:Int = 3
		#end
	
		
		If logo <> Null Then DrawImage(logo, ScreenWidth / 2, ScreenHeight / 2 - 80)
		startBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Life", startBtn.x + startBtn.w / 2, startBtn.y + startBtn.h / 6, 0.5)
		loadBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Jump", loadBtn.x + loadBtn.w / 2, loadBtn.y + loadBtn.h / 6, 0.5)
		exitBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Exit", exitBtn.x + exitBtn.w / 2, exitBtn.y + exitBtn.h / 6, 0.5)
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End

Class Puzzle
	Field state:Int[]
	
	Method New()
		state = New Int[9]
		For Local temp:Int = 1 Until 9
			state[GetEmptyIndex()] = temp
			
		Next
		For Local temp:Int = 0 Until 9
			Local poop:Int[] = ToBinaryArray(state[temp], 4)
			Print state[temp]
			For Local p:Int = 0 Until poop.Length()
				'Print
			Next
		Next
	End
	
	Method GetEmptyIndex:Int()
		While True
			Local r:int = Rnd(9)
			If state[r] = 0 Then Return r
		Wend
	End
End
