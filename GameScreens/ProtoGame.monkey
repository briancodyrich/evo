Strict

Import idle_prototype
	
Class ProtoGameScreen Extends nScreen
	Const ROWS:Int = 18
	Const COLS:Int = 20
	Const OFFSET:Int = 50
	Const MAX_POPULATION:Int = 500
	Field lastPos:Int[2]
	Field cycles:int
	Field timer:Int
	Field year:Int
	Field low:Int = 10000
	Field high:Int = 0
	Field average:int
	Field counter:int
	Field paused:bool
	Field time:int
	Field life_fit:float
	Const CYCLE_TIME:Int = 1
	Field grabMob:Life
	Field timeMult:Int = 0
	Field evoSkip:Bool
	Field board:GameBoard
	Field panel:Panel
	Field hasRendered:bool
	Method New()
	'	board = New GameBoard(50, 50, 1000, 800)
	'	While life.Length() < 100
		'	life.Push(New Life())
	'	Wend
		paused = True
		board = New GameBoard(50, 50)
		panel = New Panel(50, 50, 1050, 1000, board)
	End
	Method Init:Int()
		Return 0
	End
	
	Method MobUpdate:Void()
		
		
	End
	
	
	Method Update:Int()
		panel.Update()
		#rem
		If Game.Cursor.Hit()
			For Local l:Life = EachIn life
				If l.isPointOver(Game.Cursor.x(), Game.Cursor.y())
					grabMob = l
					Exit
				End
			Next
		End
		#end
		If grabMob <> Null and grabMob.isDead()
			grabMob = Null
		End
	'	Print v / (2 * PI)
		If KeyHit(KEY_ESCAPE)
			Game.DoSwitch(Screens.MainMenu)
		End
		
		If KeyHit(KEY_A)
		'	life.sortBy = LifeStack.AGE
		'	life.Sort()
		'	grabMob = life.Get(0)
		End
		If KeyHit(KEY_S)
		'	life.sortBy = LifeStack.SCORE
		'	life.Sort()
		'	grabMob = life.Get(0)
		End
		
		#rem
		If evoSkip and hasRendered
			hasRendered = False
			Local ms:Int = Millisecs()
			While life.Length() < 250 and (Millisecs() -ms) < 10000
				'Print(Millisecs() -ms)
				MobUpdate()
			Wend
			If life.Length() >= 250
				evoSkip = False
			End
		Else
			For Local temp:Int = 0 Until (60 * timeMult) + 1
				MobUpdate()
			Next
		End
		#end
		#rem
		If KeyHit(KEY_ENTER)
			life.sortBy = LifeStack.SCORE
			life.Sort()
			Local kill:Int = life.Length() / 2
			Print "kill: " + kill
			While life.Length() > kill
				Local index:Int = Rnd(life.Length())
				If Rnd() < index / float(life.Length())
					life.Remove(index)
				End
			Wend
			Print "done: " + life.Length()
			#rem
			For Local temp:Int = 0 Until life.Length()
				life.Get(temp).brain.debug = True
			Next
			#end
		End
		
		#end
		
		
		counter += 1
		#rem
		If KeyHit(KEY_ENTER)' or counter >= 900
			counter = 0
			life.Sort()
'			For Local temp:Int = 0 Until life.Length()
'				life.Get(temp).debug = True
'				life.Get(temp).Update()
'			Next
			
			Print life.Get(0).fitness
			While life.Length() > 20
				life.Pop()
			Wend
			While life.Length() < 80
				life.Push(New Life(life.Get(Rnd(20))))
			Wend
			While life.Length() < 100
				life.Push(New Life())
			Wend
			For Local temp:Int = 0 Until life.Length()
				life.Get(temp).Reset()
			Next
		End
		#end 
		
		
		
		
		#rem
		If timer > CYCLE_TIME
			timer = 0
			life.Sort(True)
			While life.Length() > 250
				life.Remove(life.Length() -1)
			Wend
			Local x:Int = 0
			While life.Length() < 500
				life.Push(New Life(life.Get(x)))
				x += 1
			Wend
			For Local temp:Int = 0 Until life.Length()
				life.Get(temp).Reset()
			Next
		End
		#end
		#rem
		If KeyDown(KEY_SPACE)
			For local temp:Int = 0 Until 10
				cycles += 1
				While life.Length() > 250
					life.Remove(life.Length() -1)
				Wend
				Local x:Int = 0
				While life.Length() < 500
					life.Push(New Life(life.Get(x)))
					x += 1
				Wend
				life.Sort(True)
			Next
			
			
		End
		#end 

		Return 0
	End
	
	Method Render:Int()
		panel.Render()
		hasRendered = True
		Local offset:Int = 120
		Local i:Incrementer = New Incrementer(30, 30)
		Local sec:Int = (board.time / 60) mod 60
		Local min:Int = (board.time / 3600) mod 60
		Local hr:Int = (board.time / 216000) mod 24
				

		Local ss:Stack<TypeData>

		For Local x:Int = 0 Until board.life.Length()
			If board.life.Get(x).isType()
				If ss = Null
					ss = New Stack<TypeData>()
				End
				Local type:String = board.life.Get(x).getType()
				Local found:Bool
				For Local temp:Int = 0 Until ss.Length()
					If ss.Get(temp).isMatch(type)
						ss.Get(temp).Increment()
						found = True
						Exit
					End
				Next
				If not found
					ss.Push(New TypeData(type))
				End
			End
		Next

		SetColor(0, 0, 0)
	'	DrawRect(1080, 10, 1000, 2000)
		ResetColorAlpha()
		PrettyStats(1100, i.getNext(), offset, "Time: ", PadNumber(hr, 2) + ":" + PadNumber(min, 2) + ":" + PadNumber(sec, 2))
		PrettyStats(1100, i.getNext(), offset, "Pop: ", board.life.Length())
		Local age:Float
		Local fitness:int
		Local gen:Float
		
		For Local l:Life = EachIn board.life
			age += l.age
			fitness += l.GetScore()
			gen += l.gen
		Next
		age /= board.life.Length()
		fitness /= board.life.Length()
		gen /= board.life.Length()
		sec = (age / 60) mod 60
		min = (age / 3600) mod 60
		PrettyStats(1100, i.getNext(), offset, "Score: ", fitness)
		PrettyStats(1100, i.getNext(), offset, "Ave Age: ", PadNumber(min, 2) + ":" + PadNumber(sec, 2))
		PrettyStats(1100, i.getNext(), offset, "Ave Gen: ", SigDig(gen, 2))
		If ss <> Null
			Local i2:Incrementer = New Incrementer(30, 30)
			For Local temp:Int = 0 Until ss.Length()
				PrettyStats(1100 + 300, i2.getNext(), offset, ss.Get(temp).name, ss.Get(temp).count)
			Next
		End
		'Game.white_font.Draw("Time: " + PadNumber(hr, 2) + ":" + PadNumber(min, 2) + ":" + PadNumber(sec, 2), 1100, i.getNext())
		If grabMob <> Null
			i.getNext()
			sec = (grabMob.age / 60) mod 60
			min = (grabMob.age / 3600) mod 60
			If grabMob.isType()
				PrettyStats(1100, i.getNext(), offset, "Species: ", grabMob.getType())
			End
			PrettyStats(1100, i.getNext(), offset, "Age: ", PadNumber(min, 2) + ":" + PadNumber(sec, 2))
			PrettyStats(1100, i.getNext(), offset, "Food: ", SigDig(grabMob.fitness, 2))
			PrettyStats(1100, i.getNext(), offset, "Fittness: ", grabMob.GetScore())
			PrettyStats(1100, i.getNext(), offset, "Gen: ", grabMob.gen)
			PrettyStats(1100, i.getNext(), offset, "Children: ", grabMob.children)
			i.getNext()
			Local c:Int = 0
			
		End
		#rem
		Local best:Life = life.Get(0)
		Local output:Float[] = best.brain.GetOutput([1.0, 0, 0, 0])
		Game.white_font.Draw("Score: " + best.GetFitness(), 0, 0)
		Game.white_font.Draw("Generation: " + best.gen, 0, 30)
		Game.white_font.Draw("Cycles: " + cycles, 0, 60)
		
		Game.white_font.Draw(output[0], 0, 90)
		output = best.brain.GetOutput([0, 1.0, 0, 0])
		Game.white_font.Draw(output[0], 0, 120)
		
		'Game.white_font.Draw(output[1], 0, 120)
		'Game.white_font.Draw(output[2], 0, 150)
	'	Game.white_font.Draw(output[3], 0, 180)
		Game.white_font.Draw("Num: " + best.num, 0, 220)
		#end
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
