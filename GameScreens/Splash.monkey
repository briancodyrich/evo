Strict

Import idle_prototype


Class SplashScreen extends nScreen
	Field delay:Int = 90
	Field logo:Image

	Method New()
		Init()
	End Method
	
	Method Init:Int()
		logo = LoadImage("SplashIcon.png",, Image.MidHandle)
		Print "Starting..."
		Return 0
	End Method
	
	Method Update:Int()
			delay -= 1
			
			If delay = 0 Then Game.SwitchScreen(Screens.MainMenu)
			
			If delay = 1 Then  'Load really long-loading permanent assets!
				Print("Loading vwidth_fonts...")
			
				Game.white_font = New CSFont("fonts/mp_white_black_border",,, 1.0)
				Game.plain_white_font = New CSFont("fonts/mp_plain_white",,, 1.0)
				Game.gold_font = New CSFont("fonts/mp_golden_black_border",,, 1.0)
			End If
			Return 0
	End Method
	
	Method Render:Int()
		Cls()
		Return 0
	End Method
End Class