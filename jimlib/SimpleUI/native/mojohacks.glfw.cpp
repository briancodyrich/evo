
class MojoHacks
{
	public:
	
	static int GetMouseWheel()
	{
		return s_mouseWheelVal;
	}
	
	static int MouseWheelScrolled()
	{
		return s_mouseWheelVal != s_lastMouseWheelVal;
	}
	
	static int MouseWheelChange()
	{
		return s_mouseWheelVal - s_lastMouseWheelVal;
	}
	
	static void BeginMouseWheelUpdate()
	{
		s_mouseWheelVal = glfwGetMouseWheel();
	}
	
	static void FinishMouseWheelUpdate()
	{
		s_lastMouseWheelVal = s_mouseWheelVal;
	}
	
	static int s_mouseWheelVal;
	static int s_lastMouseWheelVal;
};

int MojoHacks::s_mouseWheelVal = 0;
int MojoHacks::s_lastMouseWheelVal = 0;