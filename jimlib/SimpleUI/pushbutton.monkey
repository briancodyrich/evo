Import mojo
Import InputPointers 
Import widget

'Summary:  A basic button class which many controls derive from.
Class PushButton Extends Widget
	Field hit:Bool 'Click event indicator
	Field down:Bool 'MouseDown event indicator
	Field up:Bool 'MouseUp event indicator
	
	Method New(x:Float, y:Float, w:Float, h:Float, Input:InputPointer)
		Super.New(x, y, w, h, Input)
	End Method

	Method Render:Void(xOffset:Float = 0, yOffset:Float = 0)
		If Visible 
			SetColor(128,128,192)
			If down Then SetColor(128, 192, 192)
	
			DrawRect(x + xOffset, y + yOffset, w, h)
			SetColor(255, 255, 255)
			DrawText(Self.Text, (x + w / 2) + xOffset, (y + h / 2) + yOffset, 0.5, 0.5)
		End If
	End Method

	Method Poll:Void(xOffset:Float = 0, yOffset:Float = 0)
		hit = False
		down = False
		up = False
		Super.Poll(xOffset, yOffset)
	End Method	

	Method MouseClick:Void()
		Super.MouseClick()
		If Visible And Enabled Then hit = True
	End Method
	
	Method MouseDown:Void()
		Super.MouseDown()
		If Visible And Enabled Then down = True
	End Method
	
	Method MouseUp:Void()
		Super.MouseUp()
		If Visible and Enabled Then up = True
	End Method
End Class


'Summary: button which highlights when things are over it
Class MenuButton Extends PushButton
	Field imgDown:Image
	Field imgOver:Image
	Field imgStd:Image  'not down or over, standard
	
	Field over:Bool 'Is mouse over?
	
	'Summary:  Don't set images, but set dimensions only	
	Method New(x:Float, y:Float, w:Float, h:Float, Input:InputPointer = Game.Cursor)
		Super.New(x, y, w, h, Input)
	End Method

	'Summary:  Set Images manually	
	Method New(x:Float, y:Float, w:Float, h:Float, standard:Image, over:Image, down:Image, input:InputPointer = Game.Cursor)
		Super.New(x, y, w, h, input)
		SetImages(standard, over, down)
	End Method	

	'Summary:  Set Images automatically by postfixing the assetName.
	Method New(x:Float, y:Float, w:Float, h:Float, atlas:TextureAtlas, assetName:String, input:InputPointer = Game.Cursor)
		Super.New(x, y, w, h, input)
		Local down:Image = atlas.GetImage(assetName + "_down")
		Local over:Image = atlas.GetImage(assetName + "_over")
		Local standard:Image = atlas.GetImage(assetName + "_normal")
		SetImages(standard, over, down)
	End Method		
		
	Method SetImages(standard:Image, over:Image, down:Image)
		'Sets up the images for this button
		imgDown = down; imgOver = over; imgStd = standard
	End Method
		
	Method MouseOver:Void()
		over = True
	End Method

	Method Poll:Void(xOffset:Float = 0, yOffset:Float = 0)
		over = False
		Super.Poll(xOffset, yOffset)		
	End Method
		
	Method Render:Void(xOffset:Float = 0, yOffset:Float = 0)
	If Visible
		If down  'user is clicking
			DrawImage(imgDown, x + xOffset, y + yOffset, 0)
	  #If TARGET <> "ios", "android"
		ElseIf over  'cursor is over the control
			DrawImage(imgOver, x + xOffset, y + yOffset, 0)
	  #End
		Else  'not pushing down or over
			DrawImage(imgStd, x + xOffset, y + yOffset, 0)
		End If
	End If
	End Method
	
	Method RenderScaled:Void(xOffset:Float = 0, yOffset:Float = 0, Scale:Float = 1)
		If imgDown = Null Or imgOver = Null Or imgStd = Null
			Print "missing image"
			return
		End
		If down  'user is clicking
			DrawImage(imgDown, x + xOffset, y + yOffset, 0, Scale, Scale)
	  #If TARGET <> "ios", "android"
		ElseIf over  'cursor is over the control
			DrawImage(imgOver, x + xOffset, y + yOffset, 0, Scale, Scale)
	  #End
		Else  'not pushing down or over
			DrawImage(imgStd, x + xOffset, y + yOffset, 0, Scale, Scale)
		End If
	End Method
End Class

Class SpellButton Extends PushButton
	Field imgDown:Image
	Field imgOver:Image
	Field imgStd:Image  'not down or over, standard
	Field spells:Inventory
	Field name:String
	Field castable:Bool
	Field popup:DataBox
	
	Field over:Bool 'Is mouse over?
	
	'Summary:  Don't set images, but set dimensions only	
	Method New(x:Float, y:Float, w:Float, h:Float, Input:InputPointer = Game.Cursor, userDat:Inventory, name:string)
		Super.New(x, y, w, h, Input)
		spells = userDat
		popup = New DataBox
		Self.name = name
	End Method
	
	Method TestForCast:Void()
		castable = spells.CanCastSpell(name)
	End

	'Summary:  Set Images manually	
	Method New(x:Float, y:Float, w:Float, h:Float, standard:Image, over:Image, down:Image, input:InputPointer = Game.Cursor)
		Super.New(x, y, w, h, input)
		SetImages(standard, over, down)
	End Method	

	'Summary:  Set Images automatically by postfixing the assetName.
	Method New(x:Float, y:Float, w:Float, h:Float, atlas:TextureAtlas, assetName:String, input:InputPointer = Game.Cursor)
		Super.New(x, y, w, h, input)
		Local down:Image = atlas.GetImage(assetName + "_down")
		Local over:Image = atlas.GetImage(assetName + "_over")
		Local standard:Image = atlas.GetImage(assetName + "_normal")
		SetImages(standard, over, down)
	End Method		
		
	Method SetImages(standard:Image, over:Image, down:Image)
		'Sets up the images for this button
		imgDown = down; imgOver = over; imgStd = standard
	End Method
		
	Method MouseOver:Void()
		over = True
	End Method

	Method Poll:Void(xOffset:Float = 0, yOffset:Float = 0)
		over = False
		Super.Poll(xOffset, yOffset)		
	End Method
		
	Method Render:Void(xOffset:Float = 0, yOffset:Float = 0)
	If castable = False Then SetAlpha(0.3)
	If Visible
		If down  'user is clicking
			DrawImage(imgDown, x + xOffset, y + yOffset, 0)
	  #If TARGET <> "ios", "android"
		ElseIf over  'cursor is over the control
			DrawImage(imgOver, x + xOffset, y + yOffset, 0)
	  #End
		Else  'not pushing down or over
			DrawImage(imgStd, x + xOffset, y + yOffset, 0)
		End If
	End If
	Game.white_font.Resize(0.5)
	Game.white_font.Draw(name, x + imgStd.Width() / 2, y + imgStd.Height() / 2 - 18, 0.5)
	Game.white_font.RestoreSize()
	SetAlpha(1.0)
	End Method
	
	Method RenderScaled:Void(xOffset:Float = 0, yOffset:Float = 0, Scale:Float = 1)
		If down  'user is clicking
			DrawImage(imgDown, x + xOffset, y + yOffset, 0, Scale, Scale)
	  #If TARGET <> "ios", "android"
		ElseIf over  'cursor is over the control
			DrawImage(imgOver, x + xOffset, y + yOffset, 0, Scale, Scale)
	  #End
		Else  'not pushing down or over
			DrawImage(imgStd, x + xOffset, y + yOffset, 0, Scale, Scale)
		End If
	End Method
End Class
