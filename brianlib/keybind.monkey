Strict

Import idle_prototype


Class KeyBind
	
	Const BLOCK_MOUSE:Bool = True 'block rebinding to mouse buttons
	Const REMOVE_DUPLICATES:Bool = True 'sets duplicate values to null
	Const OVERRIDE:Bool = False 'routes calls back to KeyHit() and KeyDown()
	
	Field rebind:IntMap<Int>
	Field lockedKeys:IntStack
	
	Method New()
		lockedKeys = New IntStack()
		
		
		'keys that you can not overwrite
		lockedKeys.Push(KEY_ESCAPE)
		
		
		rebind = New IntMap<Int>
		RestoreDefaults()
	End
	
	Method OnSave:JsonObject()
		Local keyArr:JsonArray = New JsonArray(rebind.Count() * 2)
		Local counter:Int = 0
		For Local key:Int = EachIn rebind.Keys()
			keyArr.SetInt(counter, key)
			counter += 1
			keyArr.SetInt(counter, rebind.Get(key))
			counter += 1
		Next
		Local data:JsonObject = New JsonObject()
		data.Set("rebinds", keyArr)
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject)
		Local keyArr:JsonArray = JsonArray(data.Get("rebinds"))
		For Local temp:Int = 0 Until keyArr.Length() Step 2
			If rebind.Contains(keyArr.GetInt(temp))
				rebind.Set(keyArr.GetInt(temp), keyArr.GetInt(temp + 1))
			End
		Next
	End
	
	Method PrintBinds:Void()
		For Local key:Int = EachIn rebind.Keys()
			Print key + "-" + rebind.Get(key)
		Next
	End
	
	Method GetMojoKey:int()
		For Local i:int = 0 Until 512
			If input.KeyHit(i) = 1
				Return i
			End
		Next
		Return 0
	End
	
	Method KeyDown:Int(key:Int)
		If OVERRIDE
			Return input.KeyDown(key)
		Else If rebind.Contains(key)
			Return input.KeyDown(rebind.Get(key))
		Else
			Return 0
		End
	End
	
	Method KeyHit:Int(key:Int)
		If OVERRIDE
			Return input.KeyHit(key)
		Else If rebind.Contains(key)
			Return input.KeyHit(rebind.Get(key))
		Else
			Return 0
		End
	End
	
	Method areKeysUnbound:Bool()
		For Local key:Int = EachIn rebind.Keys()
			If rebind.Get(key) = 0
				Return True
			End
		Next
		Return False
	End
	
	Method RestoreDefaults:Void()
		rebind.Clear()
		For Local temp:Int = 0 Until lockedKeys.Length()
			SetSelf(lockedKeys.Get(temp))
		Next
		'Hotbar items
		SetSelf(KEY_A)
		SetSelf(KEY_S)
		SetSelf(KEY_D)
		SetSelf(KEY_F)
		SetSelf(KEY_G)
		SetSelf(KEY_1)
		SetSelf(KEY_2)
		SetSelf(KEY_3)
		SetSelf(KEY_4)
		SetSelf(KEY_5)
		
		SetSelf(KEY_SHIFT) 'Modifier key
		SetSelf(KEY_SPACE) 'Pause
		
		'Debug stuff
		SetSelf(KEY_ENTER)
		SetSelf(KEY_F1)
		SetSelf(KEY_P)
		SetSelf(KEY_D)
		SetSelf(KEY_I)
		SetSelf(KEY_H)
		SetSelf(KEY_END)
		SetSelf(KEY_HOME)
	End
	
	Method SetSelf:Void(key:Int)
		rebind.Add(key, key)
	End
	
	
	Method RebindKey:Bool(default_bind:Int, new_bind:int)
		If lockedKeys.Contains(new_bind) Or lockedKeys.Contains(default_bind)
			Return False
		Else If Not rebind.Contains(default_bind)
			Return False
		Else If BLOCK_MOUSE And (new_bind = 1 Or new_bind = 2 Or new_bind = 3)
			Return False
		Else
			rebind.Set(default_bind, new_bind)
			If REMOVE_DUPLICATES
				For Local key:Int = EachIn rebind.Keys()
					If key <> default_bind
						If rebind.Get(key) = new_bind
							rebind.Set(key, 0)
						End
					End
				Next
			End
			Return True
		End
	End
	
	Method BindingToString:String(key:Int)
		If Not rebind.Contains(key)
			Return "NO KEY"
		End
		Select rebind.Get(key)
			Case 0 Return "None"
			
			Case KEY_LMB Return "LMB"
			Case KEY_RMB Return "RMB"
			Case KEY_MMB Return "MMB"
			
			Case KEY_BACKSPACE Return "Back"
			Case KEY_TAB Return "Tab"
			Case KEY_ENTER Return "Enter"
			Case KEY_SHIFT Return "Shift"
			Case KEY_CONTROL Return "Ctrl"
			Case KEY_ESCAPE Return "Esc"
			Case KEY_SPACE Return "Space"
			Case KEY_PAGEUP Return "Page Up"
			Case KEY_PAGEDOWN Return "Page Down"
			Case KEY_END Return "End"
			Case KEY_HOME Return "Home"
			Case KEY_LEFT Return "Left"
			Case KEY_UP Return "Up"
			Case KEY_RIGHT Return "Right"
			Case KEY_DOWN Return "Down"
			Case KEY_INSERT Return "Insert"
			Case KEY_DELETE Return "Delete"

			Case KEY_0 Return "0"
			Case KEY_1 Return "1"
			Case KEY_2 Return "2"
			Case KEY_3 Return "3"
			Case KEY_4 Return "4"
			Case KEY_5 Return "5"
			Case KEY_6 Return "6"
			Case KEY_7 Return "7"
			Case KEY_8 Return "8"
			Case KEY_9 Return "9"

			Case KEY_A Return "A"
			Case KEY_B Return "B"
			Case KEY_C Return "C"
			Case KEY_D Return "D"
			Case KEY_E Return "E"
			Case KEY_F Return "F"
			Case KEY_G Return "G"
			Case KEY_H Return "H"
			Case KEY_I Return "I"
			Case KEY_J Return "J"
			Case KEY_K Return "K"
			Case KEY_L Return "L"
			Case KEY_M Return "M"
			Case KEY_N Return "N"
			Case KEY_O Return "O"
			Case KEY_P Return "P"
			Case KEY_Q Return "Q"
			Case KEY_R Return "R"
			Case KEY_S Return "S"
			Case KEY_T Return "T"
			Case KEY_U Return "U"
			Case KEY_V Return "V"
			Case KEY_W Return "W"
			Case KEY_X Return "X"
			Case KEY_Y Return "Y"
			Case KEY_Z Return "Z"

			Case KEY_NUM0 Return "KP 0"
			Case KEY_NUM1 Return "KP 1"
			Case KEY_NUM2 Return "KP 2"
			Case KEY_NUM3 Return "KP 3"
			Case KEY_NUM4 Return "KP 4"
			Case KEY_NUM5 Return "KP 5"
			Case KEY_NUM6 Return "KP 6"
			Case KEY_NUM7 Return "KP 7"
			Case KEY_NUM8 Return "KP 8"
			Case KEY_NUM9 Return "KP 9"
			Case KEY_NUMMULTIPLY Return "KP *"
			Case KEY_NUMADD Return "KP +"
			Case KEY_NUMSLASH Return "KP /"
			Case KEY_NUMSUBTRACT Return "KP -"
			Case KEY_NUMDECIMAL Return "KP ."
			Case KEY_NUMDIVIDE Return "KP /" 'what is the difference between keypad slash and keypad divide?

			Case KEY_F1 Return "F1"
			Case KEY_F2 Return "F2"
			Case KEY_F3 Return "F3"
			Case KEY_F4 Return "F4"
			Case KEY_F5 Return "F5"
			Case KEY_F6 Return "F6"
			Case KEY_F7 Return "F7"
			Case KEY_F8 Return "F8"
			Case KEY_F9 Return "F9"
			Case KEY_F10 Return "F10"
			Case KEY_F11 Return "F11"
			Case KEY_F12 Return "F12"

			Case KEY_SEMICOLON Return ";"
			Case KEY_EQUALS Return "="
			Case KEY_COMMA Return ","
			Case KEY_MINUS Return "-"
			Case KEY_PERIOD Return "."
			Case KEY_SLASH Return "/"
			Case KEY_TILDE Return "~~"
			Case KEY_OPENBRACKET Return "["
			Case KEY_BACKSLASH Return "\"
			Case KEY_CLOSEBRACKET Return "]"
			Case KEY_QUOTES Return "~q"

		End
		Return String.FromChar(rebind.Get(key))
	End
End
