Strict

Import GameStuff.Particle.photonManager


Class Steam Abstract
	Private
	Global steam:Image
	
	Public
	Function Init:Void(particleAtlas:TextureAtlas)

	End
	Function Init:Void()
		steam = LoadImage("particles/golden.png",, Image.MidHandle)
	End
	Function Flush:Void()

	End
	Function Create:Void(p:Photon)
		p.alpha = 0.06
		p.lifeTimer = 0
		p.dy = Rnd(0, 1)
		p.img = steam
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
	'	p.x -= p.dx
	'	p.y += Rnd(-2.2)
		p.lifeTimer += 1
		If p.lifeTimer > 50
			p.alpha -= 0.01
			p.y -= p.dy
			p.dx += Rnd(-1, 1)
		Else
			p.alpha += 0.01

		End
		If p.alpha < 0.05 Then p.alive = False
		If p.x < 0 or p.x > ScreenWidth Then p.alive = False
		If p.y < 0 or p.y > ScreenHeight Then p.alive = False
		If p.lifeTimer < 0 Then p.alive = False
	End
End