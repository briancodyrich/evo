Strict

Import GameStuff.Particle.photonManager


Class Text Abstract
	Private
	
	Public
	Function Init:Void(particleAtlas:TextureAtlas)

	End
	Function Init:Void()
	End
	Function Flush:Void()

	End
	Function Create:Void(p:Photon, str:String)
		p.alpha = 1.0
		p.lifeTimer = 0
		p.str = str
		p.dy = 0.15
	End
	Function Render:Void(p:Photon)
		SetBlend(AlphaBlend)
		SetAlpha(p.alpha)
		Game.white_font.Draw(p.str, p.x, p.y, 0.5)
		SetAlpha(1.0)
		SetBlend(AdditiveBlend)
	End
	Function Update:Void(p:Photon)
		p.lifeTimer += 1
		p.y -= p.dy
		If p.lifeTimer > 50
			p.alpha -= 0.02
		Else

		End
		If p.alpha < 0.05 Then p.alive = False
		If p.x < 0 or p.x > ScreenWidth Then p.alive = False
		If p.y < 0 or p.y > ScreenHeight Then p.alive = False
	End
End