
Import space_game

Class SubPhoton
	Field x:float, y:float
	Field type:int
	Field alive:bool
	Field isCommand:bool
	Field mirrored:bool
	Method SetStart:Void(x:Int, y:Int, mirrored:Bool, type:int)
		Self.alive = True
		Self.x = x
		Self.y = y
		Self.type = type
		Self.mirrored = mirrored
		Self.isCommand = False
	End
	Method SetStart:Void(x:Int, y:Int, mirrored:Bool, type:int, isCommand:bool)
		Self.alive = True
		Self.x = x
		Self.y = y
		Self.type = type
		Self.mirrored = mirrored
		Self.isCommand = isCommand
	End
	Method GetType:Int()
		Return type
	End
	Method GetX:Int()
		Return x
	End
	Method GetY:Int()
		Return y
	End
	Method hasDemands:Bool()
		Return isCommand
	End
	
End

Class Photon Extends SubPhoton
	Const MAX_SUB_PHOTON:Int = 10
	Field x0:Float, y0:Float 'Start
	Field x1:Float, y1:Float 'Mid
	Field x2:Float, y2:Float 'End
	Field dx:Float, dy:Float
	Field id:int
	Field gravity:Float
	Field theta:float
	Field speed:Float
	Field alpha:float
	Field specialBehavior:bool
	Field effectTimer:float
	Field lifeTimer:float
	Field img:Image
	
	Field toEmit:SubPhoton[MAX_SUB_PHOTON]
	Field emitSize:int
	Field obj:Object
	Field str:String
	
	Method New()
		For Local x:Int = 0 Until toEmit.Length()
			toEmit[x] = New SubPhoton()
		Next
	End
	
	Method hasSubPhotons:Bool()
		If emitSize > 0 Return True
		Return False
	End
	
	Method addSubPhoton:Void(x:Int, y:Int, type:int, isCommand:Bool = False)
		If emitSize < toEmit.Length()
			toEmit[emitSize].SetStart(x, y, type, isCommand)
			emitSize += 1
		End
	End
	
	Method Copy:Void(p:Photon)
		x = p.x
		y = p.y
		x0 = p.x0
		y0 = p.y0
		x1 = p.x1
		y1 = p.y1
		x2 = p.x2
		y2 = p.y2
		dx = p.dx
		dy = p.dy
		id = p.id
		gravity = p.gravity
		theta = p.theta
		speed = p.speed
		alpha = p.alpha
		specialBehavior = p.specialBehavior
		isCommand = p.isCommand
		type = p.type
		effectTimer = p.effectTimer
		lifeTimer = p.lifeTimer
		img = p.img
		alive = p.alive
		
		emitSize = p.emitSize
		toEmit = p.toEmit
		obj = p.obj
		str = p.str
		mirrored = p.mirrored
	End
End