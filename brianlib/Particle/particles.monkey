Import space_game

#rem
	Field x:Int, y:int
	Field x0:Float, y0:Float 'Start
	Field x1:Float, y1:Float 'Mid
	Field x2:Float, y2:Float 'End
	Field dx:Float, dy:Float
	Field speed:Float
	Field alpha:float
	Field type:Int
	Field effectTimer:float
	Field lifeTimer:Int
	Field img:Image
#END




Class EmberPhoton Abstract
	Private
	Global aura1:Image
	Global aura2:Image
	Global aura3:Image
	
	Public
	Function Init(particleAtlas:TextureAtlas)
		aura1 = particleAtlas.GetImage("pt_aura_1", 1, Image.MidHandle)
		aura2 = particleAtlas.GetImage("pt_aura_2", 1, Image.MidHandle)
		aura3 = particleAtlas.GetImage("pt_aura_3", 1, Image.MidHandle)
	End
	Function Flush:Void()
		aura1 = Null
		aura2 = Null
		aura3 = Null
	End
	Function Create:Void(p:Photon)
		p.dy = Rnd(2, 4)
		p.lifeTimer = Rnd(30, 60)
		Select int(Rnd(1, 4))
			Case 1
				p.img = aura1
			Case 2
				p.img = aura2
			Case 3
				p.img = aura3
		End
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(0.4)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
	'	If Rnd(0, 100) > 95 Then p.addSubPhoton(p.x, p.y, p.type)
		p.x += Rnd(-1, 1)
		p.y += p.dy * dt.delta
		p.lifeTimer -= 1 * dt.delta
		If p.x < 0 or p.x > ScreenWidth Then p.alive = False
		If p.y < 0 or p.y > ScreenHeight Then p.alive = False
		If p.lifeTimer < 0 Then p.alive = False
	End
End


Class CardDustPhoton Abstract
	Private
	Global aura2:Image
	Global aura3:Image
	
	Public
	Function Init(particleAtlas:TextureAtlas)
		aura2 = particleAtlas.GetImage("pt_aura_2", 1, Image.MidHandle)
		aura3 = particleAtlas.GetImage("pt_aura_3", 1, Image.MidHandle)
	End
	Function Create:Void(p:Photon)
		p.x0 = p.x
		p.y0 = p.y
		p.x2 = p.x + Rnd(300)
		p.y2 = p.y - Rnd(300)
		p.x1 = p.x + Rnd(300)
		p.y1 = p.y - Rnd(300)
		p.effectTimer = 0.00
		p.alpha = Rnd(0.3, 0.8)
		p.x = GetBezier(p.x0, p.x1, p.x2, p.effectTimer)
		p.y = GetBezier(p.y0, p.y1, p.y2, p.effectTimer)
		Select int(Rnd(1, 3))
			Case 1
				p.img = aura2
			Case 2
				p.img = aura3
		End
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.effectTimer += 0.02 * dt.delta
		p.alpha -= 0.015 * dt.delta
		p.x = GetBezier(p.x0, p.x1, p.x2, p.effectTimer)
		p.y = GetBezier(p.y0, p.y1, p.y2, p.effectTimer)
		If p.x < 0 or p.x > ScreenWidth Then p.alive = False
		If p.y < 0 or p.y > ScreenHeight Then p.alive = False
		If p.alpha <= 0.05 Then p.alive = False
		
	End
End

Class RisePhoton Abstract
	Private
	Global aura2:Image
	Global aura3:Image
	
	Public
	Function Init(particleAtlas:TextureAtlas)
		aura2 = particleAtlas.GetImage("pt_aura_2", 1, Image.MidHandle)
		aura3 = particleAtlas.GetImage("pt_aura_3", 1, Image.MidHandle)
	End
	Function Create:Void(p:Photon)
		p.dy = Rnd(2, 4)
		p.alpha = Rnd(0.3, 0.8)
		Select int(Rnd(1, 3))
				Case 1
					p.img = aura2
				Case 2
					p.img = aura3
			End
		
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.x += Rnd(-1, 1)
		p.y -= p.dy * dt.delta
		p.alpha -= 0.01 * dt.delta
		If p.alpha <= 0.05 Then p.alive = False
	End
End

Class TrailPhoton Abstract
	Private
	Global golden:Image
	
	Public
	Function Init(particleAtlas:TextureAtlas)
		golden = particleAtlas.GetImage("golden", 1, Image.MidHandle)
	End
	Function Create:Void(p:Photon)
		p.alpha = Rnd(0.3, 0.5)
		p.lifeTimer = Rnd(40, 80)
		p.img = golden
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.lifeTimer -= 1 * dt.delta
		If p.lifeTimer > 11
			p.alpha += 0.01 * dt.delta
		Else
			p.alpha -= 0.05 * dt.delta
		End
		If p.alpha < 0 Then p.alpha = 0
		If p.alpha >= 0.5 Then p.alpha = 0.3
		If p.lifeTimer < 0 Then p.alive = False
	End
End

Class BlownTrailPhoton Abstract
	Public
	Function Create:Void(p:Photon)
		p.type = PhotonTypes.BlownTrailPhoton
		p.x0 = p.x
		p.y0 = p.y
		p.x2 = p.x0 - Rnd(300)
		p.y2 = p.y0 - Rnd(300)
		p.x1 = p.x0 + Rnd(300)
		p.y1 = p.y0 + Rnd(300)
		p.effectTimer = 0.0
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.effectTimer += 0.01 * dt.delta
		p.alpha -= 0.004 * dt.delta
		p.x = GetBezier(p.x0, p.x1, p.x2, p.effectTimer)
		p.y = GetBezier(p.y0, p.y1, p.y2, p.effectTimer)
		If p.alpha <= 0.1 Then p.alive = False
	End
End

Class OMGlobPhoton2 Abstract
	Public
		Global glow:Image
		Global trail:Image
	Function Init(particleAtlas:TextureAtlas)
		glow = LoadImage("graphics/particles/particle_golden_orb.png",, Image.MidHandle)
		trail = LoadImage("graphics/particles/particle_golden_trail_1.png",, Image.MidHandle)
	End
	Function Create:Void(p:Photon)
		p.x0 = p.x
		p.y0 = p.y
		p.specialBehavior = False
		p.alpha = 0.3
		p.theta = 0
		p.x2 = p.x0
		p.y2 = p.y0
		p.x1 = p.x0 + Rnd(-400, 400)
		p.y1 = p.y0 + Rnd(-400, 400)
		p.effectTimer = 0.0
	End
	Function Render:Void(p:Photon)
	'	If p.img = Null Then Return
		SetAlpha(p.alpha)
		PushMatrix
			Translate(p.x, p.y)
			Rotate(p.theta)
			Rotate(120)
			PushMatrix
			'	Rotate(180)
			'	Translate(trail.Width / 2, trail.Height / 2)
				DrawImage(trail, 0, 0)
			PopMatrix
			Rotate(120)
			PushMatrix
		'		Rotate(180)
			'	Translate(trail.Width / 2, trail.Height / 2)
				DrawImage(trail, 0, 0)
			PopMatrix
			Rotate(120)
			PushMatrix
			'	Rotate(180)
			'	Translate(trail.Width / 2, trail.Height / 2)
				DrawImage(trail, 0, 0)
			PopMatrix
		'		Rotate(120)
		'	DrawImage(trail, +trail.Width / 2, 0)
		'		Rotate(120)
		'	DrawImage(trail, +trail.Width / 2, 0)
			DrawImage(glow, 0, 0)
			
			
		PopMatrix
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
	    If p.specialBehavior Then p.effectTimer += 0.03 * dt.delta
		p.alpha -= 0.002 * dt.delta
		p.theta -= 10 * dt.delta
		p.x = GetBezier(p.x0, p.x1, p.x2, p.effectTimer)
		p.y = GetBezier(p.y0, p.y1, p.y2, p.effectTimer)
		If p.alpha <= 0.1 Then p.alive = False
	End
End

Class OMGlobPhoton Abstract
	Public
		Global glow:Image
		Global trail:Image
	Function Init(particleAtlas:TextureAtlas)
		glow = LoadImage("graphics/particles/particle_golden_orb.png",, Image.MidHandle)
		'trail = LoadImage("graphics/particles/particle_golden_trail_2.png",, Image.MidHandle)
		trail = particleAtlas.GetImage("pt_blue_line", 1, Image.MidHandle)
	End
	Function Create:Void(p:Photon)
		p.x0 = p.x
		p.y0 = p.y
		p.alpha = 1
		p.theta = 0
		p.x2 = p.x0 '
		p.y2 = p.y0 - 600
		p.x1 = p.x0 + Rnd(400)
		p.y1 = p.y0 - Rnd(600, 900)
		p.effectTimer = 0.0
	End
	Function Render:Void(p:Photon)
	'	If p.img = Null Then Return
		SetAlpha(p.alpha)
		PushMatrix
			Translate(p.x, p.y)
			Rotate(p.theta)
			Rotate(120)
			PushMatrix
			'	Rotate(180)
			'	Translate(trail.Width / 2, trail.Height / 2)
				DrawImage(trail, 0, 0)
			PopMatrix
			Rotate(120)
			PushMatrix
		'		Rotate(180)
			'	Translate(trail.Width / 2, trail.Height / 2)
				DrawImage(trail, 0, 0)
			PopMatrix
			Rotate(120)
			PushMatrix
			'	Rotate(180)
			'	Translate(trail.Width / 2, trail.Height / 2)
				DrawImage(trail, 0, 0)
			PopMatrix
		'		Rotate(120)
		'	DrawImage(trail, +trail.Width / 2, 0)
		'		Rotate(120)
		'	DrawImage(trail, +trail.Width / 2, 0)
			DrawImage(glow, 0, 0)
			
			
		PopMatrix
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
	'	If Rnd(0, 100) > 20 Then
		p.addSubPhoton(p.x, p.y, PhotonTypes.OMGlobPhoton2)
		p.effectTimer += 0.01 * dt.delta
		p.alpha -= 0.004 * dt.delta
		p.theta -= 10 * dt.delta
		p.x = GetBezier(p.x0, p.x1, p.x2, p.effectTimer)
		p.y = GetBezier(p.y0, p.y1, p.y2, p.effectTimer)
		If p.alpha <= 0.1 Then p.alive = False
		If p.effectTimer >= 1
			For Local temp:Int = 0 To 7
				p.addSubPhoton(p.x, p.y, PhotonTypes.OMGlobPhoton2)
			Next
			p.alive = False
			p.addSubPhoton(p.x, p.y, PhotonManager.CONVERT_OMGLOB, True)
		End
	End
End

Class SparkPhoton Abstract
	Private
	Global fire:Image
	Const GRAVITY:Float = 1.5 '0.05
	Public
	Function Init(particleAtlas:TextureAtlas)
		fire = particleAtlas.GetImage("fireball_small", 1, Image.MidHandle)
	End
	Function Create:Void(p:Photon, fromRight:bool)
'		p.lifeTimer
		p.alpha = 0.5
		p.x0 = p.x
		p.y0 = p.y
		p.dy = 1.01
		p.dx = Rnd(50, 70)
		p.img = fire
		If fromRight
			p.theta = Rnd(60, 70)
		Else
			p.theta = Rnd(100, 110)
		End
'		p.alpha = 0.5
'		p.dx = Rnd(5, 15)
'		If Not fromRight Then p.dx = - (p.dx)
'		p.dy = -Rnd(10, 20)
'		p.lifeTimer = Rnd(30, 70)
'		p.gravity = 0
'		p.img = fire
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.x += (Cos(p.theta) * p.dx) * dt.delta
		p.y += (-Sin(p.theta) * p.dx + p.dy) * dt.delta
		p.dy = (p.dy + GRAVITY) * dt.delta
	'	If p.dy >= 80 Then p.dy = 80
		If p.y > ScreenHeight Then p.alive = False
	'	If p.alpha <= 0.05 Then p.alive = False
	End
End

Class GoldPhoton Abstract
	Private
	Global coin:Image
	Public
	Function Init(particleAtlas:TextureAtlas)
		coin = LoadImage("graphics/cardgame/gold_icon.png")
	End
	Function Create:Void(p:Photon, coins:int)
'		p.lifeTimer
		p.alpha = 0.8
		p.x0 = p.x
		p.y0 = p.y
		p.dy = -2
		p.theta = coins '//to lazy to add a variable the coin number is theta
		p.img = coin
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		Game.gold_font.Draw("+" + int(p.theta), p.x + 100, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.y += p.dy * dt.delta
		p.dy += 0.1 * dt.delta
		If p.dy > 0 Then p.alpha -= 0.02 * dt.delta
		If p.alpha <= 0.05
			p.alive = False
			FaeEngine.Enqueue(New QueueItem(EngineCode.SoundEffect, SoundEffect.PLAY_SIMPLE_SOUND, SoundEffect.GET_GOLD))
			FaeEngine.Enqueue(New QueueItem(EngineCode.Inventory, Inventory.ADD_GOLD, BoxInt(p.theta)))
		End
	End
End









Class SparkPhotonold Abstract
	Private
	Global fire:Image
	Const GRAVITY:Float = 0.05
	Const TERMINALV:Float = 1.5
	Const FRICTION:Float = 0.05
	Public
	Function Init(particleAtlas:TextureAtlas)
		fire = particleAtlas.GetImage("fireball_small", 1, Image.MidHandle)
	End
	Function Create:Void(p:Photon, fromRight:bool)
		p.alpha = 0.5
		p.dx = Rnd(5, 15)
		If Not fromRight Then p.dx = - (p.dx)
		p.dy = -Rnd(10, 20)
		p.lifeTimer = Rnd(30, 70)
		p.gravity = 0
		p.img = fire
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
'		p.x += Cos(p.theta) * p.dx
'		p.y += -Sin(p.theta) * p.dy
'		p.dx = Abs(p.dx - 0.1)
'		If p.theta > - 90 And p.theta < 90 Then p.theta -= 3
'		If p.theta > 90 And p.theta < 270 Then p.theta += 3
		p.gravity += GRAVITY
		If p.gravity >= TERMINALV Then p.gravity = TERMINALV
		If p.dx > 0
			p.dx -= FRICTION
		Else
			p.dx += FRICTION
		End
		If p.dy > 0
			p.dy -= FRICTION
			p.dy += p.gravity
		Else
			p.dy += FRICTION
			p.dy += p.gravity
		End
		p.x += p.dx
		p.y += p.dy
		
		p.lifeTimer -= 1
		If p.lifeTimer <= 0
			p.alpha -= 0.03
		End
		If p.alpha <= 0.05 Then p.alive = False
	End
End

Class BallisticPhoton Abstract
	Private
	Global fire:Image
	Const GRAVITY:Float = 0.7
	Public
	Function Init(particleAtlas:TextureAtlas)
		fire = particleAtlas.GetImage("fireball_small", 1, Image.MidHandle)
	End
	Function Create:Void(p:Photon)
		p.alpha = 0.9
		p.x0 = p.x
		p.y0 = p.y
		p.dy = 1.01
		p.dx = Rnd(30, 35)
		p.img = fire
		p.theta = Rnd(60, 70)
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
'		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.x += (Cos(p.theta) * p.dx) * dt.delta
		p.y += (-Sin(p.theta) * p.dx + p.dy) * dt.delta
		p.dy = p.dy + GRAVITY * dt.delta
	'	If p.dy >= 80 Then p.dy = 80
		If p.y > ScreenHeight Then p.alive = False
	'	If p.alpha <= 0.05 Then p.alive = False
	End
End











