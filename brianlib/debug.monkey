Strict

Import space_game

Class Constants
	Global editor:Bool
	Global loadsamoney:Bool
	
	Function Set:Void(data:JsonObject)
		If data.Contains("editor") Then editor = data.GetBool("editor")
		If data.Contains("loadsamoney") Then loadsamoney = data.GetBool("loadsamoney")
	End
End