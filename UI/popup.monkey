Strict

Import idle_prototype

Class Popup
	Const OK:Int = 1
	Const CANCEL:Int = 2
	Field msg:Stack<String>
	Field Btn1:MenuButton
	Field Btn2:MenuButton
	Field Btn3:MenuButton
	Field Btn1Msg:String
	Field Btn2Msg:String
	Field Btn3Msg:String
	Field Btn1Type:int
	Field Btn2Type:int
	Field Btn3Type:int
	Field popupMsg:Int
	Field alpha:Float
	Field id:String
	Field frozen:bool
	Field code:Int
	
	Method New(id:String, msg:Stack<String>, Btn1Msg:String = "", Btn1Type:Int = 0, Btn2Msg:String = "", Btn2Type:Int = 0, Btn3Msg:String = "", Btn3Type:Int = 0)
		Self.msg = msg
		Self.Btn1Msg = Btn1Msg
		Self.Btn1Type = Btn1Type
		Self.Btn2Msg = Btn2Msg
		Self.Btn2Type = Btn2Type
		Self.Btn3Msg = Btn3Msg
		Self.Btn3Type = Btn3Type
		frozen = True
		Btn1 = New MenuButton( (ScreenWidth / 2) - 258, (ScreenHeight / 2) + 95, 160, 56)
		Btn1.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		Btn2 = New MenuButton( (ScreenWidth / 2) - 84, (ScreenHeight / 2) + 95, 160, 56)
		Btn2.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		Btn3 = New MenuButton( (ScreenWidth / 2) + 90, (ScreenHeight / 2) + 95, 160, 56)
		Btn3.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		Self.id = id
		alpha = 0.1
	End
	
	Method Update:Void()
		If Not frozen Then Return
		If Btn1Msg <> "" And Btn1Type <> 0
			Btn1.Poll()
			If Btn1.hit
				frozen = False
				code = Btn1Type
			End
		End
		If Btn2Msg <> "" And Btn2Type <> 0
			Btn2.Poll()
			If Btn2.hit
				frozen = False
				code = Btn2Type
			End
		End
		If Btn3Msg <> "" And Btn3Type <> 0
			Btn3.Poll()
			If Btn3.hit
				frozen = False
				code = Btn3Type
			End
		End
		
		
		
	End
	
	Method isActiveMessage:Bool(id:String)
		If Self.id = id Then Return True
		Return False
	End
	
	Method getMessageCode:Int()
		Return code
	End
	
	'	Method messageReady
	
	Method isScreenFrozen:Bool()
		Return frozen
	End
	Method Render:Void()
		'If Not frozen And dead Then Return
		SetAlpha(0.5)
		SetColor(0, 0, 0)
		DrawRect(0, 0, ScreenWidth, ScreenHeight)
		alpha = Min(alpha + 0.05, 1.0)

		DrawDialogWindow( (ScreenWidth / 2) - 258, (ScreenHeight / 2) - 150, 516, 300, 0, 0, 0, Min(alpha, 0.75))
		SetAlpha(alpha)
		For Local temp:Int = 0 Until msg.Length()
			Game.white_font.Draw(msg.Get(temp), (ScreenWidth / 2), (ScreenHeight / 2) - 140 + (temp * 35), 0.5)
		Next
		
		If Btn1Msg <> "" And Btn1Type <> 0
			Btn1.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(Btn1Msg, Btn1.x + Btn1.w / 2, Btn1.y + Btn1.h / 6, 0.5)
		End
		If Btn2Msg <> "" And Btn2Type <> 0
			Btn2.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(Btn2Msg, Btn2.x + Btn2.w / 2, Btn2.y + Btn2.h / 6, 0.5)
		End
		If Btn3Msg <> "" And Btn3Type <> 0
			Btn3.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(Btn3Msg, Btn3.x + Btn3.w / 2, Btn3.y + Btn3.h / 6, 0.5)
		End
		ResetColorAlpha()
	End
End
