Strict

#GLFW_WINDOW_TITLE="Idle Prototype"
#GLFW_WINDOW_WIDTH=1280
#GLFW_WINDOW_HEIGHT=720
#GLFW_WINDOW_RESIZABLE=True
#GLFW_WINDOW_FULLSCREEN=false

#OPENGL_DEPTH_BUFFER_ENABLED=True

#TEXT_FILES="*.txt|*.xml|*.json|*.fnt"


Import mojo

'Internal engine components
Import jimlib ''Import all jimlib modules
Import angelfont_exts ''Still using CSFont as a class name

'Import UI
Import UI.contextMenu 'currently unused
Import UI.popup
Import UI.paintbrush

'Import Screens
Import GameScreens.Splash
Import GameScreens.MainMenu
Import GameScreens.ProtoGame


Import AI.brain
Import AI.life
Import AI.tile
Import AI.food
Import AI.gene
Import AI.world

Import board.gameboard
Import board.tile
Import board.panel

'Import functions
Import brianlib.keybind

'Import Components
Import Components.component
Import Components.componentInterface
Import Components.gameBoard
Import Components.buffer
Import Components.item
Import Components.belt

'Import Component Types
Import Components.Types.cpucore
Import Components.Types.solarPanel
Import Components.Types.coalmine
Import Components.Types.trash



Global dt:DeltaTimer

Function Main:Int()
	New Game()
	Return 0
End



Class Game Extends App
	Field letterbox:Image 'Lettebox image
	Const PRINT_CLICK:bool = True
	Global gold_font:CSFont
	Global white_font:CSFont
	Global plain_white_font:CSFont
	Field bg:Image
	
	
	'Generic image assets
	Global default_img:Image  'Placeholder image if something goes wrong
	
	'Screen switching / transitioning
	Global CurrentScreen:nScreen 'Current screen. Switching this forces a screen change and unloads the previous screen from RAM.
	Global CurrentScreenNumber:Int  'This is set by the SwitchScreen function.
	Global LastScreenNumber:Int
	Global RootScreenNumber:int
	Global LastRootScreenNumber:Int
	Global FadeIn:ScaledFadeTransition, FadeOut:ScaledFadeTransition
	Global fadeState:Int  'Signals state for fade in/out. 0: None; 1: Out; 2-3: Loading; 4: In
	
	Global InputPaused:Bool = False
	Global Cursor:GamepadPointer = New GamepadPointer  'Game cursor, for UI elements!
 	Global Keys:KeyBind = New KeyBind()
	
	Global ScreenToSwitchTo:Int 'Holding variable; next screen after transition
	Global SwitchCondition:String  'General argument holding variable for screen switch.  EG: Map location
	
	Global Audio:AudioSystem

	Global showFPS:Bool = True
	Global showPC:Bool = false
	Global lastMillisec:int
	Global lastFPS:int
	Global FPSCount:Int
	
	Global popup:Popup
	Global isFrozen:Bool

	Method OnCreate:Int()
		SetUpdateRate 60
		dt = New DeltaTimer(60)
		RandomizeSeed()
		initAutoScale(1920, 1080, True) 'Start autoscale
		Cursor.Init()
		Audio.Init()
		bg = LoadImage("bg.jpg")
		letterbox = LoadImage("letterbox.png")
		default_img = LoadImage("default.png")
		FadeIn = New ScaledFadeTransition(0, 0, 0, True)
		FadeOut = New ScaledFadeTransition(0, 0, 0, False)
		CurrentScreen = New SplashScreen()   'Bootstrap the game.
		
		Return 0
	End

	Method OnUpdate:Int()
		Cursor.Poll()  'Updates cursor state and positions
		If PRINT_CLICK
			If Game.Cursor.Hit()
				Print Game.Cursor.x() + "/" + Game.Cursor.y()
			End
		End
		
		#If TARGET="glfw"
		If KeyHit(KEY_CLOSE)
			Stop()
		EndIf
		#End
	
		
		
		dt.UpdateDelta()
		If Game.popup <> Null And Game.popup.isScreenFrozen()
			isFrozen = True
			Game.popup.Update()
			Return 0
		Else
			isFrozen = False
		End
		
		
		CurrentScreen.Update()

		'Update the transition faders.
		FadeOut.Update()
		FadeIn.Update()
		
		Return 0
	End
	
	Method OnRender:Int()
		Cls()
		ScaleScreen()
		If showFPS
			If Millisecs() - (lastMillisec) > 1000
				lastMillisec = Millisecs()
				lastFPS = FPSCount
				FPSCount = 0
			Else
				FPSCount += 1
			End
		End
		
		UpdateAsyncEvents()
		CurrentScreen.Render()  'Render the current screen.
		'Render and check the faders.
		Select fadeState
			Case FadeStates.OUT
				If FadeOut.Finished Then  'Need to switch screens.
					FadeOut.RenderFullFrame()
					DoSwitch(ScreenToSwitchTo, SwitchCondition)
					fadeState = FadeStates.LOADING  'Prepare to switch to a fade-in next frame.
				End If
			
			Case FadeStates.LOADING
				FadeOut.RenderFullFrame()
				fadeState = FadeStates.FINISHED_LOADING

			Case FadeStates.FINISHED_LOADING
				FadeIn.Start(250)
				FadeIn.RenderFullFrame()
				FadeOut.Reset()
				fadeState = FadeStates.IN
			
			Case FadeStates.IN
				If FadeIn.Finished Then  'Return input to normal
					'InputPaused = False
					FadeIn.Reset()
					fadeState = 0
				End If
		End Select
		
		Cursor.Draw()
					
		FadeIn.Render()
		FadeOut.Render()  'This is below the check to ensure Finished frame is rendered at resource load.
		
	
		
		If Game.popup <> Null And Game.popup.isScreenFrozen()
			Game.popup.Render()
		End
		Return 0
	End
	
	Method DoTexturedLetterbox:Void()
		Local lbSize:Float = SlideAmount * ScaleFactor  'Letterbox size
	
		SetMatrix(1, 0, 0, 1, 0, 0); SetAlpha(1)
		SetScissor(0, 0, lbSize, DeviceHeight())  'Left Letterbox
		
		For Local y:Int = 0 To DeviceHeight() Step 512
			For Local x:Int = 0 To DeviceWidth() / 2 Step 256
				DrawImage(letterbox, x, y)
			Next
		Next
		
		SetScissor(DeviceWidth() -lbSize, 0, Ceil(lbSize), DeviceHeight())  'Right letterbox
		
		For Local y:Int = 0 To DeviceHeight() Step 512
			For Local x:Int = DeviceWidth() / 2 To DeviceWidth() Step 256
				DrawImage(letterbox, x, y)
			Next
		Next
		
		SetAlpha(0.2); SetColor(0, 0, 0)
		DrawRect(DeviceWidth() -lbSize, 4.5, 4.5, DeviceHeight())
		SetAlpha(1); SetColor(255, 255, 255)
		
	End Method
	
	Function SwitchScreen:Void(ScreenNumber:Int, Condition:String = "") 'Starts the fader to switch, with a string arg (for maps)
		If Game.popup <> Null And Game.popup.isScreenFrozen()
			Return
		End
		InputPaused = True
		ScreenToSwitchTo = ScreenNumber
		SwitchCondition = Condition
		FadeOut.Start(150)
		fadeState = FadeStates.OUT
	End Function
	
	Function SetRootScreen:Void(ScreenNumber:Int)
		If RootScreenNumber = ScreenNumber Then Return
		LastRootScreenNumber = RootScreenNumber
		RootScreenNumber = ScreenNumber
	End
	
	Function DoSwitch:Int(ScreenNumber:Int, Condition:String = "") 'Manual switch;  instant.  Try to avoid doing this
		CurrentScreen = Null  'Dereference screen.
		If ScreenNumber = Screens.Root
			ScreenNumber = RootScreenNumber
		End
		If ScreenNumber = Screens.LastRoot
			ScreenNumber = LastRootScreenNumber
		End
		If ScreenNumber = Screens.LastScreen
			ScreenNumber = LastScreenNumber
		End
		LastScreenNumber = CurrentScreenNumber
		Select ScreenNumber
			Case Screens.Splash
				CurrentScreen = New SplashScreen()
			Case Screens.MainMenu
				CurrentScreen = New MainMenuScreen()
			Case Screens.ProtoGame
				CurrentScreen = New ProtoGameScreen()
			Default
				Error("Screen '" + ScreenNumber + "' not found")
		End Select
		
		CurrentScreenNumber = ScreenNumber
		
		Return CurrentScreenNumber
	End Function
		
	Function Stop:Void()  'Exit App
		Error("")
	End Function
End Class

Class Screens 'Enum
	Const LastScreen:Int = -3
	Const LastRoot:Int = -2
	Const Root:Int = -1
	Const Splash:Int = 0
	Const MainMenu:Int = 1
	Const ProtoGame:Int = 2
End Class



	
'Transition
Class ScaledFadeTransition Extends Transition
	Field r:Int, g:Int, b:Int  'Fade color
	Field fadeIn:Bool 'Am I fading in?

	Method New(r:Int, g:Int, b:Int, FadeIn:Bool = False)
		Self.r = r
		Self.g = g
		Self.b = b
		fadeIn = FadeIn
	End Method

	Method Render:Void()
		Super.Render()  'Required
		If Started = False And Finished = False Then Return
	
		SetColor(r, g, b)
		If fadeIn Then SetAlpha(1 - Percent) Else SetAlpha(Percent)
		DrawRect(DeviceX(0) - 1, DeviceY(0) - 1, 2 + DeviceX(1) - DeviceX(0), 2 + DeviceY(1) - DeviceY(0))
		SetColor(255, 255, 255); SetAlpha(1)
			
		'If Finished and fadeIn = False Then DrawText("Loading...", DeviceX(0) + 8, DeviceY(0) + 8)
	End Method
	
	Method RenderFullFrame:Void()
		SetColor(r, g, b)
		DrawRect(DeviceX(0) - 1, DeviceY(0) - 1, 2 + DeviceX(1) - DeviceX(0), 2 + DeviceY(1) - DeviceY(0))
		SetColor(255, 255, 255)
		'If fadeIn = False Then DrawText("Loading...", DeviceX(0) + 8, DeviceY(0) + 8)
	End Method
	
End Class

	
		
Function Stop:Void()  'Exit App
	Error("")
End Function

'Summary:  Sets the randomizer's seed to a value based on the current time.
Function RandomizeSeed:Void()
	Local time:= GetDate()
	''Ints are signed in monkey. Let's take half the MinValue of an int, then add the
	'current time and day of the month in milliseconds.
	Seed = -1073741824 + time[6] + (time[5] * 1000) + (time[4] * 60000) + (time[3] * 3600000) +
		(time[2] * 86400000) + (time[1] * $3DCC5000) + (time[0])
	#If CONFIG="debug"
	Print "Seed: " + Seed
	#End
	
End Function

Function ResetColorAlpha:Void()
	SetColor(255, 255, 255)
	SetAlpha(1.0)
End


Class DeltaTimer
	Field targetfps:Float = 60
	Field currentticks:Float
	Field lastticks:Float
	Field frametime:Float
	Field delta:Float
	
	Method New (fps:Float)
		targetfps = fps
		lastticks = Millisecs()
	End
	
	Method UpdateDelta:Void()
		currentticks = Millisecs()
		frametime = currentticks - lastticks
		delta = frametime / (1000.0 / targetfps)
		lastticks = currentticks
	End
End
Function GetPrettyTime:String(seconds:Int)
	Local min:Int = seconds / 60
	seconds -= (min * 60)
	Return PadNumber(min, 2) + ":" + PadNumber(seconds, 2)
End

Function PrettyStats:Void(x:Int, y:Int, offset:Int, stra:String, strb:String)
	Game.white_font.Draw(stra, x, y)
	Game.white_font.Draw(strb, x + offset, y)
End

Function ThrowErrorMessage:Void(type:String, msg:String)
	Local message:Stack<String>
	message = New Stack<String>
	message.Push(type)
	message.Push(msg)
	Game.popup = New Popup("Error", message,,,,, "Close", Popup.OK)
End

Function ThrowErrorMessage:Void(message:Stack<String>)
	Game.popup = New Popup("Error", message,,,,, "Close", Popup.OK)
End

Function DrawFPS:Void()
	If Game.lastFPS >= 59
		Game.white_font.Draw("60", 0, 10, 0)
	Else
		Game.white_font.Draw(Game.lastFPS, 0, 10, 0)
	End
End
