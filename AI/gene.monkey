Strict

Import idle_prototype

#rem
Class Brain
	Field genes:Stack<Gene>
	Field spec:bool
	Method New(inputs:Int, outputs:int)
		genes = New Stack<Gene>()
		Local count:Int = Rnd(GC.MIN_START, GC.MAX_START)
		For Local temp:Int = 0 Until count
			genes.Push(New Gene())
		Next
	End
	Method New(b:Brain)
		genes = New Stack<Gene>()
		For Local temp:Int = 0 Until b.genes.Length()
			If Rnd() > 0.02
				genes.Push(New Gene(b.genes.Get(temp)))
			End If
			
		Next
		If Rnd() < 0.05
			genes.Push(New Gene())
		End If
		If genes.Length() <> b.genes.Length()
			spec = True
		End
	End
	Method Think:float[] (input:Float[])
		Local r:Float[] = New Float[GC.MAX_OUTPUTS]
		For Local temp:Int = 0 Until genes.Length()
			
			genes.Get(temp).Eval(input, r)
		Next
		For Local temp:Int = 0 Until r.Length()
			r[temp] = HTan(r[temp])
		Next
		Return r
	End
	
End


Class Gene

	Const OP_AND:Int = 0
	Const OP_OR:Int = 1
	Const OP_NUM:Int = 2
	Const MUTATE_CHANCE:Float = 0.03
	
	Field varA:Int
	Field tarA:Float
	Field ranA:Float
	
	Field opp:int
	
	Field varB:Int
	Field tarB:Float
	Field ranB:Float
	
	Field outputStr:float
	Field outputVar:int
	
	Field debug:bool
	Field active:bool
	
	Method New()
		varA = Rnd(GC.MAX_INPUTS)
		tarA = Rnd(-1, 1)
		ranA = Rnd(0.5)
		
		opp = Rnd(OP_NUM)
		
		varB = Rnd(GC.MAX_INPUTS)
		tarB = Rnd(-1, 1)
		ranB = Rnd(0.5)
		
		outputStr = Rnd(-1, 1)
		outputVar = Rnd(GC.MAX_OUTPUTS)
	End
	
	Method New(g:Gene)
		varA = g.varA
		varB = g.varB
		tarA = g.tarA
		tarB = g.tarB
		ranA = g.ranA
		ranB = g.ranB
		
		opp = g.opp
	
		outputStr = g.outputStr
		outputVar = g.outputVar
		If Rnd() < MUTATE_CHANCE
			varA = Rnd(GC.MAX_INPUTS)
			tarA = Rnd(-1, 1)
			ranA = Rnd(0.5)
		
			opp = Rnd(OP_NUM)
		
			varB = Rnd(GC.MAX_INPUTS)
			tarB = Rnd(-1, 1)
			ranB = Rnd(0.5)
		
			outputStr = Rnd(-1, 1)
			outputVar = Rnd(GC.MAX_OUTPUTS)
			Return
		End
		If Rnd() < MUTATE_CHANCE Then varA = Rnd(GC.MAX_INPUTS)
		If Rnd() < MUTATE_CHANCE Then tarA = Rnd(-1, 1)
		If Rnd() < MUTATE_CHANCE Then ranA = Rnd(0.5)
		
		If Rnd() < MUTATE_CHANCE Then opp = Rnd(OP_NUM)
		
		If Rnd() < MUTATE_CHANCE Then varB = Rnd(GC.MAX_INPUTS)
		If Rnd() < MUTATE_CHANCE Then tarB = Rnd(-1, 1)
		If Rnd() < MUTATE_CHANCE Then ranB = Rnd(0.5)
		
		If Rnd() < MUTATE_CHANCE Then outputStr = Rnd(-1, 1)
		If Rnd() < MUTATE_CHANCE Then outputVar = Rnd(GC.MAX_OUTPUTS)
		
	
	End
	
	Method Eval:Void(input:Float[], output:Float[])
		active = False
		If opp = OP_AND
			If Abs(input[varA] - tarA) < ranA and Abs(input[varB] - tarB) < ranB
				output[outputVar] += outputStr
				active = True
			End
		Else If opp = OP_OR
			If Abs(input[varA] - tarA) < ranA or Abs(input[varB] - tarB) < ranB
				output[outputVar] += outputStr
				active = True
			End
		End
		If debug then DebugStop()
	End
End

#end



















