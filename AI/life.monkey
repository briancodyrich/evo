Strict

Import idle_prototype

Class Life Extends PanelObject
	Const TURN_SPEED:Float = 0.05
	Const LEFT_SENSOR:Int = 1
	Const RIGHT_SENSOR:Int = 2
	Const BACK_SENSOR:Int = 3
	Const SIZE:Int = 20
	Const VISION_RANGE:int = 160 'was 160
	Field gen:int
	Field fitness:float
	Field x:Float
	Field y:float
	Field dir:float
	Field r:Int
	Field g:Int
	Field b:int
	Field brain:Brain
	Field max:float
	Field debug:bool
	Field age:int
	Field children:int
	Field total_fitness:float
	Field mem:Float[3]
	Field isCarn:bool
	Field bite:bool
	Field inPain:bool
	Field type:String
	Method New(x:Int, y:Int, c:Camera)
		Super.New(x, y, SIZE * 2, SIZE * 2, c)
		Self.x = x
		Self.y = y
		gen = 1
		Local cArr:int[] =[int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91))]
		type = String.FromChars(cArr)
		fitness = 50
		r = Rnd(255)
		g = Rnd(255)
		b = Rnd(255)
		dir = Rnd(360)
		If Rnd() < 0.5 Then isCarn = True
		brain = New Brain()
	End
	Method GetScore:Int()
		Return (total_fitness * age) / 1000.0
	End
	Method isType:Bool()
		Return gen >= 5
	End
	Method getType:String()
		Local m:String
		If isCarn Then m = "[C]" Else m = "[H]"
		
		If gen >= 50
			Return type + m + "-" + (gen / 50)
		End
		Return type + m
	End
	Method New(l:Life)
		Super.New(l.x, l.y, SIZE * 2, SIZE * 2, l.camera)
		brain = New Brain(l.brain)
		type = l.type
		isCarn = l.isCarn
		Local dx:Float = Rnd(-60, 60)
		Local dy:Float = Rnd(-60, 60)
		
		y = l.y + dx
		x = l.x + dx
		Move(dx, dy)
		dir = Rnd(360)
	'	x = Rnd(50, 1050)
	'	y = Rnd(50, 1000)
		fitness = l.fitness
		l.children += 1
		r = l.r
		g = l.g
		b = l.b
		gen = l.gen
		If brain.mutate
			r = Clamp(l.r + int(Rnd(-10, 10)), 0, 255)
			g = Clamp(l.g + int(Rnd(-10, 10)), 0, 255)
			b = Clamp(l.b + int(Rnd(-10, 10)), 0, 255)
			gen += 1
		End
	End
	Method Update:bool(leftFood:Int, rightFood:Int, leftHerb:int, rightHerb:Int, leftCarn:Int, rightCarn:Int, backCarn:int, foodAtFeet:int, herbAtFeet:Int, carnAtFeet:int)
		age += 1
		fitness -= 0.02
		#rem
		If x > 1050 Then x -= 1050
		If x < 50 Then x += 1050
		If y > 1000 Then y -= 1000
		If y < 50 Then y += 1000
		#end
		Local input:Float[Brain.INPUT]
		input[GC.INPUT_CONST] = 1.0
		input[GC.INPUT_LEFT_FOOD] = leftFood
		input[GC.INPUT_RIGHT_FOOD] = rightFood
		input[GC.INPUT_LEFT_HERB] = leftHerb
		input[GC.INPUT_RIGHT_HERB] = rightHerb
		input[GC.INPUT_LEFT_CARN] = leftCarn
		input[GC.INPUT_RIGHT_CARN] = rightCarn
		input[GC.INPUT_BACK_CARN] = backCarn
		If foodAtFeet = 1.0 Then input[GC.INPUT_FOOD_AT_FEET] = 1.0
		If herbAtFeet = 1.0 Then input[GC.INPUT_HERB_AT_FEET] = 1.0
		If carnAtFeet = 1.0 Then input[GC.INPUT_CARN_AT_FEET] = 1.0
		If inPain Then input[GC.INPUT_IN_PAIN] = 1.0
		Local out:Float[] = brain.GetOutput(input)
		bite = False
		If out[GC.OUTPUT_BITE] <> 0
			bite = True
			fitness -= 0.2
		End
		dir -= out[GC.OUTPUT_RIGHT] * 15.0
		dir += out[GC.OUTPUT_LEFT] * 15.0
		dir = Normalize(dir)
		Local dx:float = Sin(dir) * out[GC.OUTPUT_FORWARD] * 2.0
		Local dy:Float = Cos(dir) * out[GC.OUTPUT_FORWARD] * 2.0
		fitness -= Abs(dx + dy) / 120
		x += dx
		y += dy
		Move(dx, dy)
		dx = Sin(dir) * out[GC.OUTPUT_BOOST] * 4.0
		dy = Cos(dir) * out[GC.OUTPUT_BOOST] * 4.0
		fitness -= Abs(dx + dy) / 75
		x += dx
		y += dy
		Move(dx, dy)
	'	Local t:Float = ATan2r(tx - x, ty - y) / (PI)
	'	Local dist:Float = HTan(Distance(x, y, tx, ty) / 10.0)
	'	Print dir
'		Local data:float[] = brain.Think([0.0, t, dir, mem[0], mem[1], mem[2], dist])
	'	Local target:float = data[GC.OUTPUT_VECTOR]
	'	If dir < target Then dir += TURN_SPEED
	'	If dir > target Then dir -= TURN_SPEED
	'	Local dx:float = Cosr(dir * 2 * PI) * Clamp(data[GC.OUTPUT_SPEED], 0.0, 1.0) * 10.0 * boost
	'	Local dy:Float = Sinr(dir * 2 * PI) * Clamp(data[GC.OUTPUT_SPEED], 0.0, 1.0) * 10.0 * boost
	'	Local loss:Float = (Abs(dx + dy)) / 400.0
'		fitness -= loss
	'	x += dx'Clamp(dx, -4.0, 4.0)
	'	y += dy'Clamp(dy, -4.0, 4.0)
	'	if debug then DebugStop()
	'	If debug Then DebugStop()
		Return True

	End
	Method isDead:Bool()
		Return fitness <= 0
	End
	
	Method isPointOver:Bool(x:Int, y:Int)
		Return Distance(x, y, Self.x, Self.y) < 15
	End
	
	Method getSensorReading:Int(x:Int, y:Int)
		If not insideBox(x, y) Then Return 0
		If Distance(x, y, Self.x, Self.y) > VISION_RANGE Then Return 0
		Local t:Float = ATan2(x - Self.x, y - Self.y)
		Local l:Int = Normalize(dir + 40)
		Local r:Int = Normalize(dir - 40)
		Local b:Int = Normalize(dir - 180)
		If Abs(l - t) <= 40
			Return LEFT_SENSOR
		End
		If Abs(r - t) <= 40
			Return RIGHT_SENSOR
		End
		If Abs(b - t) <= 30
			Return BACK_SENSOR
		End
		Return 0
	End
	
	Method insideBox:bool(x:Int, y:Int)
		Return Self.x - VISION_RANGE < x and x < Self.x + VISION_RANGE  and Self.y - VISION_RANGE < y and y < Self.y + VISION_RANGE 
	End
	
	Method Eat:Void(food:Int)
		fitness += food
		total_fitness += food
	End
	
	Method Bite:Void(l:Life)
		fitness += 2
		total_fitness += 2
		l.fitness -= 2
		l.inPain = True
	End

	
	Method Render:Void()
		
		If bite
			If isCarn
				SetColor(255, 0, 0)
			Else
				SetColor(0,255,0)
			End
		End
	'	Local ty:float = y
	'	Local t:Float = ATan2r(Game.Cursor.x() - x, Game. - y) / (2 * PI)
		'arctan( (m1 - m2) / (1 - (m1 * m2)))
	'	Print t
	'	dir += 0.01
	'	If dir > 1.0 Then dir = 0
		Local dx:float = Sin(dir) * 20
	    Local dy:Float = Cos(dir) * 20
		DrawCircle(CenterX(), CenterY(), SIZE)
		SetColor(r, g, b)
		'Super.Render()
		DrawCircle(CenterX(), CenterY(), SIZE - 5)
		ResetColorAlpha()
'		DrawLine(x, y, x + dx, y + dy)
	'	DrawRect(x - 10, y - 10, 20, 20)
		dx = Sin(dir) * 30
		dy = Cos(dir) * 30
	'	Print Abs(dir - t)
		'DrawLine(x, y, x + dx, y + dy)
		'x += dx / 20.0
		'y += dy / 20.0

	End
	Method Normalize:Int(theta:Int)
		If theta > 180 Then Return theta - 360
		If theta < - 180 Then Return theta + 360
		Return theta
	End
	Method GetFitness:Float()
		Return fitness
		
	End
End

Class TypeData
	Field name:String
	Field count:Int
	Method New(name:String)
		Self.name = name
		count = 1
	End
	Method isMatch:Bool(str:String)
		Return name.Compare(str) = 0
	End
	Method Increment:Void()
		count += 1
	End
End

Class LifeStack Extends Stack<Life>
	Field sortBy:Int
	Const FITNESS:Int = 0
	Const AGE:Int = 1
	Const SCORE:Int = 2
	Method New(data:Life[])
		Super.New( data )
	End

	Method Compare:Int(lhs:Life, rhs:Life)
		If sortBy = FITNESS
			If lhs.GetFitness() > rhs.GetFitness() Then Return - 1
			If lhs.GetFitness() < rhs.GetFitness() Then Return 1
		End
		If sortBy = SCORE
			If lhs.GetScore() > rhs.GetScore() Then Return - 1
			If lhs.GetScore() < rhs.GetScore() Then Return 1
		End
		If sortBy = AGE
			If lhs.age > rhs.age Then Return - 1
			If lhs.age < rhs.age Then Return 1
		End
		Return 0
	End

End