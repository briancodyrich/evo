Strict

Import idle_prototype

Class Food Extends PanelObject
	Field x:Int
	Field y:Int
	Field val:float
	Method New(x:Int, y:Int, c:Camera)
		Super.New(x, y, 10, 10, c)
		Self.x = x
		Self.y = y
		val = 100
	End
	Method Eat:float()
		If val > 0
			val -= 2.0
			Return 2.0
		End
		Return 0
	End
	Method IsDead:Bool()
		Return val <= 0
	End
	Method Render:Void()
		SetColor(0, 255, 0)
		DrawCircle(CenterX(), CenterY(), 5)
		ResetColorAlpha()
	End
End