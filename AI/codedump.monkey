Class Network
	Const MAX_MEMORY:Int = 60
	Const SAMPLE_RATE:Float = 0.1
	Const FIRE_T:float = 0.70
	Field hidden:Node[]
	Field output:Node[]
	Field memory:Memory[]
	Field current:int
	Field debug:bool
	Method New(n:Network)
		hidden = New Node[n.hidden.Length()]
		output = New Node[n.output.Length()]
		For Local temp:Int = 0 Until hidden.Length()
			hidden[temp] = New Node(n.hidden[temp])
		Next
		For Local temp:Int = 0 Until output.Length()
			output[temp] = New Node(n.output[temp])
		Next
	
	End
	Method P:Void()
		Print "hidden"
		'		For Local temp = 0 Until hidden.Length()
			
		'	Next
	End
	Method New(input:Int, hidden:Int, output:Int)
		Self.hidden = New Node[hidden]
		For Local temp:Int = 0 Until Self.hidden.Length()
			Self.hidden[temp] = New Node(input + 1)
		Next
		Self.output = New Node[output]
		For Local temp:Int = 0 Until Self.output.Length()
			Self.output[temp] = New Node(hidden + 1)
		Next
		memory = New Memory[MAX_MEMORY]
		For Local temp:Int = 0 Until memory.Length()
			memory[temp] = New Memory(input, output)
		Next

	End
	
	Method GetOutput:Float[] (input:Float[], remember:Bool = False)
		Local i2:float[hidden.Length()]
		Local out:Float[output.Length()]
		
		For Local temp:Int = 0 Until hidden.Length()
			i2[temp] = hidden[temp].GetOutput(input)
		Next
		
		'Print "output layer"
		For Local temp:Int = 0 Until output.Length()
			out[temp] = output[temp].GetOutput(i2)
		End
		If remember
			memory[current].Set(input, out)
			current = Clamp(current + 1, 0, MAX_MEMORY - 1)
		EndIf
	
		Return out
	End
	Method memoryFull:Bool()
		Return current >= MAX_MEMORY - 1
	End
	Method ProcessMemories:Void(isPos:Bool, count:int)
		For Local temp:Int = 0 Until count
			Local i:Int = Rnd(memory.Length())
			memory[i].Process(isPos)
			Think(memory[i].input, memory[i].output)
		Next
		current = 0
		
		
	End
	#rem
	Method Think:Void(input:Float[], val:Float)
	Local i2:float[hidden.Length() +1]
	For Local temp:Int = 0 Until hidden.Length() +1
	If temp = hidden.Length()
	i2[temp] = 1.0
	Else
	i2[temp] = hidden[temp].GetOutput(input)
	End
			
	Next
	'Print "train output"
	For Local x:Int = 0 Until Self.output.Length()
	'			Local dA:Float[] = Self.output[x].TrainOutputStatic(i2, val)
	'	Print dA.Length()
	'	Print "train input"
	For Local z:Int = 0 Until hidden.Length()
	'		hidden[z].TrainHidden(input, dA[z])
	Next
	Next
	End
	#end
	Method Think:Void(input:Float[], ideal:Float[])
		'If debug Then DebugStop()
		Local i2:float[hidden.Length() +1]
		For Local temp:Int = 0 Until hidden.Length() +1
			If temp = hidden.Length()
				i2[temp] = 1.0
			Else
				i2[temp] = hidden[temp].GetOutput(input)
			End
			
		Next
		'Print "train output"
		For Local x:Int = 0 Until Self.output.Length()
			Local dA:Float[] = Self.output[x].TrainOutput(i2, ideal[x])
			'	Print "ideal[x]: " + ideal[x]
			'	Print dA.Length()
			'	Print "train input"
			For Local z:Int = 0 Until hidden.Length()
				hidden[z].TrainHidden(input, dA[z])
			Next
		Next
	End
End

Class Memory
	Field input:Float[]
	Field output:Float[]
	Const RATE:Float = 1.0
	Method New(input:Int, output:Int)
		Self.input = New Float[input]
		Self.output = New Float[output]
	End
	Method Set:Void(input:Float[], output:Float[])
		For Local temp:Int = 0 Until Self.input.Length()
			Self.input[temp] = input[temp]
		Next
		For Local temp:Int = 0 Until Self.output.Length()
			Self.output[temp] = output[temp]
		Next
		'	DebugStop()
	End
	Method Process:Void(isPos:bool)
		Local i:int
		Local max:Float
		For Local temp:Int = 0 Until output.Length()
			If output[temp] > max
				i = temp
				max = output[temp]
			EndIf
		Next
		For Local temp:Int = 0 Until output.Length()
			If isPos
				If i = temp
					output[temp] += 0.1
				Else
					output[temp] -= 0.1
				End
			Else
				If i = temp
					output[temp] -= 0.1
				Else
					output[temp] += 0.1
				End
			End
		Next
		If isPos
			For Local temp:Int = 0 Until output.Length()
				If output[temp] > Network.FIRE_T
					output[temp] += 0.1
				Else
					output[temp] -= 0.1
				End
			Next
		Else
			For Local temp:Int = 0 Until output.Length()
				If output[temp] > Network.FIRE_T
					output[temp] -= 0.1
				Else
					output[temp] += 0.1
				End
			End
		End
				
		#rem
		For Local temp:Int = 0 Until output.Length()
		If isPos
		If temp = i
		output[i] += 0.1
		Else
		output[i] -= 0.1
		End
		Else
				
		End
			
		Next
		#end
		If isPos
			'	output[i] = 1.0
		Else
		
		End
		
	End
End

Class Node
	Const MUTATE_CHANCE:float = 0.2
	Const MAX_VAL:float = 5.0
	Const MIN_VAL:Float = -5.0
	Const MIN_MUTATE:Float = -0.15
	Const MAX_MUTATE:Float = 0.15
	Const LEARNING_RATE:Float = 1.0
	Const DEAD_RATE:Float = 0.9
	Field weights:Float[]
	Method New(input:Int)
		weights = New Float[input]
		For Local temp:Int = 0 Until input
			If Rnd() > DEAD_RATE
				weights[temp] = Rnd(MIN_VAL, MAX_VAL)
			End
		Next
	End
	
	Method New(n:Node)
		weights = New Float[n.weights.Length()]
		For Local temp:Int = 0 Until weights.Length()
			If Rnd(1) < MUTATE_CHANCE
				weights[temp] = Clamp(n.weights[temp] + Rnd(MIN_MUTATE, MAX_MUTATE), MIN_VAL, MAX_VAL)
			Else
				weights[temp] = n.weights[temp]
			End
		Next
	End
	Method GetOutput:Float(input:Float[])
		Local r:float = 0.0
		For Local temp:Int = 0 Until weights.Length()
			'	Print temp
			If temp = weights.Length() -1
				r += 1.0 * weights[temp]
			Else
				r += input[temp] * weights[temp]
			End
		Next
		Return GetSigmoid(r)
	End
	#rem
	Method TrainOutputStatic:float[] (input:Float[], ideal:float)
	Local r:Float[weights.Length()]
	Local output:Float = GetOutput(input)
	Local err:Float = (0.77 - output) * ideal
	For Local temp:Int = 0 Until weights.Length()
	Local nodeDelta:Float = err * GetSigDer(output)
	r[temp] = nodeDelta * weights[temp]
	Local grad:Float = nodeDelta * input[temp]
	weights[temp] += grad
	Next
	Return r
	End
	#end
	Method TrainOutput:float[] (input:Float[], ideal:float)
		Local r:Float[weights.Length()]
		Local output:Float = GetOutput(input)
		'	Print "output " + output
		Local err:Float = (ideal - output) * LEARNING_RATE
		'	Print "E: " + err
		'	Print "S"
		For Local temp:Int = 0 Until weights.Length()
			Local nodeDelta:Float = err * GetSigDer(output)
			r[temp] = nodeDelta * weights[temp]
			'	Print "weight " + weights[temp]
			'Print "dA: " + r[temp]
			Local grad:Float = nodeDelta * input[temp]
			'	Print "TV: " + input[temp]
			'	Print "output grad: " + grad
			'	Print "old weight: " + weights[temp]
			weights[temp] += grad
			'	Print "new weight: " + weights[temp]
		Next
		'	Print "end"
		Return r
	End
	Method TrainHidden:Void(input:Float[], dA:Float)
		Local val:Float = GetOutput(input)
		'	Print "val: " + val
		'	Print "dA: " + dA
		Local nodeDelta:Float = dA * GetSigDer(val)
		'	Print "start"
		For Local temp:Int = 0 Until weights.Length()
			Local grad:Float
			If (temp = weights.Length() -1)
				grad = nodeDelta
			Else
				grad = nodeDelta * input[temp]
			EndIf
			'	Print "hidden nodeDelta : " + nodeDelta
			'	Print "input grad: " + grad
			'	Print "old weight: " + weights[temp]
			'	Print "input " + input[temp]
			weights[temp] += grad + nodeDelta
			'	Print "new weight: " + weights[temp]
		Next
		'	Print "end"
	End
End


Class Cluster
	Field neurons:Neuron[]
	
	Method New(input:Int, output:int)
		neurons = New Neuron[output]
		For Local x:Int = 0 Until output
			neurons[x] = New Neuron(input)
		Next
	End
	
	Method New(b:Brain)
		neurons = New Neuron[b.neurons.Length()]
		For Local x:Int = 0 Until neurons.Length()
			neurons[x] = New Neuron(b.neurons[x])
		Next
	End
	
	Method GetOutput:float[] (input:Float[])
		Local v:Float[] = New Float[neurons.Length()]
		
		For Local x:Int = 0 Until neurons.Length()
			v[x] = neurons[x].GetOutput(input)
		Next
		Return v
	End
	Method GetBoolOutput:bool[] (input:Float[])
		Local v:bool[] = New bool[neurons.Length()]
		
		For Local x:Int = 0 Until neurons.Length()
			v[x] = neurons[x].GetBoolOutput(input)
		Next
		Return v
	End
	Method Think:Void(input:Float[], output:Float[], times:Int)
		For Local x:Int = 0 Until times
			For Local y:Int = 0 Until output.Length()
				neurons[y].Train(input, output[y])
			Next
		Next
	End
	Method Reinforce:Void(input:Float[], isPos:bool, times:Int)
		For Local x:Int = 0 Until times
			For Local y:Int = 0 Until neurons.Length()
				neurons[y].Reinforce(input, isPos)
			Next
		Next
	End
	Method Reinforce:Void(input:Float[], output:Bool[], isPos:bool, times:Int)
		For Local x:Int = 0 Until times
			For Local y:Int = 0 Until neurons.Length()
				neurons[y].Reinforce(input, output[y], isPos)
			Next
		Next
	End
	
	Method Choose:Bool[] (input:Float[])
		Local v:bool[] = New bool[neurons.Length()]
		Local o:Float[] = GetOutput(input)
		Local max:Float = 0
		Local index:Int = 0
		For Local temp:Int = 0 Until o.Length()
			If o[temp] > max
				max = o[temp]
				index = temp
			End
		Next
		v[index] = True
		Return v
	End
End



Class Neuron
	Const MIN_MUTATE:float = -0.05
	Const MAX_MUTATE:Float = 0.05
	Const ACTIVATION_POINT:Float = 0.77
	Const MUTATE_CHANCE:Float = 0.8
	Const STRENGTH:Float = 0.03
	Field layers:Float[]
	Method New(size:int)
		layers = New Float[size]
		For Local x:Int = 0 Until size
			layers[x] = Rnd(0, 1)
		Next
	End
	
	Method New(n:Neuron)
		layers = New Float[n.layers.Length()]
		For Local x:Int = 0 Until layers.Length()
			If Rnd() < MUTATE_CHANCE
				layers[x] = Clamp(n.layers[x] + Rnd(MIN_MUTATE, MAX_MUTATE), -1.0, 1.0)
			Else
				layers[x] = Clamp(n.layers[x], -1.0, 1.0)
			End
		Next
	End
	
	Method GetOutput:Float(input:Float[])
		Local output:float
		For Local x:Int = 0 Until layers.Length()
			output += layers[x] * input[x]
		Next
		Return GetSigmoid(output)
	End
	
	Method GetBoolOutput:Bool(input:Float[])
		Local output:float
		For Local x:Int = 0 Until layers.Length()
			output += layers[x] * input[x]
		Next
		Return GetSigmoid(output) >= ACTIVATION_POINT
	End
	Method Train:Void(input:Float[], output:float)
		Local val:Float = GetOutput(input)
		Local err:Float = output - val
		For Local temp:Int = 0 Until layers.Length()
			Local adj:Float = err * input[temp] * GetSigDer(val)
			layers[temp] += adj
		Next
	End
	Method Reinforce:Void(input:Float[], isPos:bool)
		Local err:float
		If GetBoolOutput(input)
			err = 1.0
		Else
			err = -1.0
		End
		If not isPos
			err *= -1.0
		End
		For Local temp:Int = 0 Until layers.Length()
			Local adj:Float = err * input[temp] * STRENGTH
			layers[temp] += adj
		Next
	End
	Method Reinforce:Void(input:Float[], output:bool, isPos:bool)
		Local err:float
		If output
			err = 1.0
		Else
			err = -1.0
		End
		If not isPos
			err *= -1.0
		End
		For Local temp:Int = 0 Until layers.Length()
			Local adj:Float = err * input[temp] * STRENGTH
			layers[temp] += adj
		Next
	End
	
	
End