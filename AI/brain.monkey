Strict

Import idle_prototype





Class Brain
	Const INPUT:Int = 12
	Const HIDDEN:Int = 9
	Const OUTPUT:Int = 5
	Field inputA:Neuron[]
	Field inputB:Neuron[]
	Field hidden:Neuron[]
	Field mutate:Bool
	Method New()
	
		inputA = New Neuron[INPUT]
		For Local temp:Int = 0 Until inputA.Length()
			inputA[temp] = New Neuron(HIDDEN)
		Next
		
		inputB = New Neuron[INPUT]
		For Local temp:Int = 0 Until inputB.Length()
			inputB[temp] = New Neuron(OUTPUT)
		Next
		
		hidden = New Neuron[HIDDEN]
		For Local temp:Int = 0 Until hidden.Length()
			hidden[temp] = New Neuron(OUTPUT)
		Next
	End
	
	Method New(b:Brain)
		inputA = New Neuron[INPUT]
		For Local temp:Int = 0 Until inputA.Length()
			inputA[temp] = New Neuron(b.inputA[temp])
		Next
		
		inputB = New Neuron[INPUT]
		For Local temp:Int = 0 Until inputB.Length()
			inputB[temp] = New Neuron(b.inputB[temp])
		Next
		
		hidden = New Neuron[HIDDEN]
		For Local temp:Int = 0 Until hidden.Length()
			hidden[temp] = New Neuron(b.hidden[temp])
		Next
		mutate = Neuron.mutate
		Neuron.mutate = False
	End
	Method GetOutput:Float[] (input:Float[])
		Local r:Float[] = New Float[OUTPUT]
		ResetAll()
		For Local temp:Int = 0 Until inputA.Length()
			inputA[temp].Add(input[temp])
			inputB[temp].Add(input[temp])
		Next
		For Local x:Int = 0 Until hidden.Length()
			For Local y:Int = 0 Until inputA.Length()
				hidden[x].Add(inputA[y].GetOutput(x, False))
			Next
		Next
		For Local x:Int = 0 Until r.Length()
			For Local y:Int = 0 Until inputB.Length()
				r[x] += inputB[y].GetOutput(x, False)
			Next
		Next
		For Local x:Int = 0 Until r.Length()
			For Local y:Int = 0 Until hidden.Length()
				r[x] += hidden[y].GetOutput(x)
				If r[x] <= 0
					r[x] = 0
				Else
					r[x] = GetSigmoid(r[x])
				End
			Next
		Next
		
		Return r
	End
	
	Method ResetAll:Void()
		For Local temp:Int = 0 Until inputA.Length()
			inputA[temp].Reset()
		Next
		
		For Local temp:Int = 0 Until inputB.Length()
			inputB[temp].Reset()
		Next
		
		For Local temp:Int = 0 Until hidden.Length()
			hidden[temp].Reset()
		Next
	End
End

Class Neuron
	Field weights:Float[]
	Field value:float
	Global mutate:bool
	Const CONNECT_CHANCE:Float = 0.5
	Const MUTATE_CHANCE:Float = 0.05
	Const CONNECTION_CHANGE_CHANCE:Float = 0.1
	Const NEW_CONNECTION_CHANCE:Float = 0.02
	Const MAX_MUTATE:Float = -0.2
	Const MIN_MUTATE:Float = 0.2
	Const MIN_RANGE:Float = -2.0
	Const MAX_RANGE:Float = 2.0
	Method New(outputs:Int)
		weights = New Float[outputs]
		For Local temp:Int = 0 Until weights.Length()
			If Rnd() < CONNECT_CHANCE
				weights[temp] = Rnd(MIN_RANGE, MAX_RANGE)
			End
		Next
	End
	Method New(n:Neuron)
		weights = New Float[n.weights.Length()]
		For Local temp:Int = 0 Until weights.Length()
			If Rnd() < MUTATE_CHANCE
				If Rnd() < CONNECTION_CHANGE_CHANCE
					If n.weights[temp] = 0.0
						weights[temp] = Rnd(MIN_RANGE, MAX_RANGE)
						mutate = True
					Else
						weights[temp] = 0.0
						mutate = True
					End
				Else If n.weights[temp] <> 0.0
					weights[temp] = Clamp(n.weights[temp] + Rnd(MIN_MUTATE, MAX_MUTATE), MIN_RANGE, MAX_RANGE)
				Else If Rnd() < NEW_CONNECTION_CHANCE
					weights[temp] = Rnd(MIN_RANGE, MAX_RANGE)
				End
			Else
				weights[temp] = n.weights[temp]
			End
		Next
	End
	Method Reset:Void()
		value = 0.0
	End
	Method Add:Void(v:Float)
		value += v
	End
	Method GetOutput:Float(i:Int, useSigmoid:Bool = True)
		Local output:Int = value * weights[i]
		If output <= 0 Then Return 0
		If useSigmoid Then Return GetSigmoid(output)
		Return output
	End
End




Class GC
	Const INPUT_CONST:Int = 0
	Const INPUT_LEFT_FOOD:Int = 1
	Const INPUT_RIGHT_FOOD:Int = 2
	Const INPUT_LEFT_CARN:Int = 3
	Const INPUT_RIGHT_CARN:Int = 4
	Const INPUT_BACK_CARN:Int = 5
	Const INPUT_LEFT_HERB:Int = 6
	Const INPUT_RIGHT_HERB:Int = 7
	Const INPUT_FOOD_AT_FEET:Int = 8
	Const INPUT_HERB_AT_FEET:Int = 9
	Const INPUT_CARN_AT_FEET:Int = 10
	Const INPUT_IN_PAIN:Int = 11
	
	Const OUTPUT_LEFT:Int = 0
	Const OUTPUT_RIGHT:Int = 1
	Const OUTPUT_FORWARD:Int = 2
	Const OUTPUT_BOOST:Int = 3
	Const OUTPUT_BITE:Int = 4
	
	
	Const OUTPUT_MEM3:Int = 5
	'Const OUTPUT_BOOST:Int = 6
	Const OUTPUT_TARGET_VEG:Int = 7
	Const OUTPUT_TARGET_MEAT:Int = 8
	
	Const MIN_START:int = 3
	Const MAX_START:Int = 7
	
	Function GetInputName:String(x:Int)
		Select x
		
		End
		Return "NULL"
	End
	Function GetOutputName:String(x:Int)
		Select x
			Case GC.OUTPUT_MEM1
				Return "Memory 1"
			Case GC.OUTPUT_MEM2
				Return "Memory 2"
			Case GC.OUTPUT_MEM3
				Return "Memory 3"
			Case GC.OUTPUT_BOOST
				Return "Boost"
			Case GC.OUTPUT_TARGET_VEG
				Return "HERB"
			Case GC.OUTPUT_TARGET_MEAT
				Return "CARN"
				
				'	Case GC.OUTPUT_DY
				'	Return "Dy"
		End
		Return "NULL"
	End
End

























