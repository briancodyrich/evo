Strict

Import idle_prototype

Class Thought
	Field i:Stmt
	Field t:Effect
	Field e:Effect
End

Class Stmt
	Const COND_GREATER:Int = 1
	Const COND_LESSER:Int = 2
	
	Const TWENTY_PERCENT:Int = 1
	Const FOURTY_PERCENT:Int = 2
	Const SIXTY_PERCENT:Int = 3
	Const EIGHTY_PERCENT:Int = 4
	Const STAT_FOOD:Int = 5
	Const
	
	Field a:Int
	Field cond:Int
	Field b:int
End

Class Effect
	Field type:Int
	Field pow:float
End