Strict

Import idle_prototype



Class Tileold
	Const WIDTH:Int = 50
	Const HEIGHT:Int = 50
	Const MAX_FOOD:float = 128.0
	Const FOOD_CONST:float = 2
	
	Const RED:Int = 1
	Const GREEN:Int = 2
	
	Field x:Int
	Field y:Int
	Field food:int
	Method New(x:Int, y:Int, food:Int)
		Self.x = x
		Self.y = y
		Self.food = food
	End
	
	Method Reset:Void()
		food = MAX_FOOD
	End
	Method Render:Void()
	    SetColor(255, (food / MAX_FOOD) * 255, 0)
		DrawRect(x, y, WIDTH, HEIGHT)
		ResetColorAlpha
	End
	
	Function GetDaily:Float(day:Int)
		Return 2.0 + Sin(day)
	End
	
	Method EatFood:Void()
		food -= 1
	End
	Method GetFood:Float()
		Return Clamp(1.0 + (food / MAX_FOOD), 1.00, 2.0)
	End
	
	Method Update:Void(day:float)
		food = Clamp(food + GetDaily(day), 0.0, MAX_FOOD)
	End

	Method isInGrid:Bool(x:Int, y:Int)
		If x >= Self.x and x < Self.x + WIDTH and y >= Self.y and y < Self.y + HEIGHT Then Return True
		Return False
	End
End