Strict

Import idle_prototype


Class Tile Extends PanelObject
	Const LAND:Int = 0
	Const WATER:Int = 1
	Const SIZE:Int = 32
	Global land:Image
	Global water:Image
	Field x:int
	Field y:int
	Field type:int
	Method New(x:Int, y:Int, c:Camera)
		Super.New(x, y, SIZE, SIZE, c)
		Self.x = x
		Self.y = y
		type = LAND
		If water = Null
			water = LoadImage("water.png",, Image.MidHandle)
		End
		If land = Null
			land = LoadImage("land.png",, Image.MidHandle)
		End
	End
	Method Render:Void()
		If type = LAND
			If land = Null
				Super.Render()
			Else
				DrawImage(land, CenterX(), CenterY())
			End
		Else If type = WATER
			If water = Null
				Super.Render()
			Else
				DrawImage(water, CenterX(), CenterY())
			End
		End
	End
End