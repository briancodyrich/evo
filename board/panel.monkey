Strict

Import idle_prototype


Class Panel

	Const THROW_THRESHOLD:Int = 15 'the speed you have to be going to trigger the movement on mouse release
	Const FRICTON:Float = 2.2 '
	Const DEFAULT_SPEED:Int = 30 'min movement speed
	Const FRICTION_MULT:Float = 0.8 'for throwing at edge MUST BE < 1
	Const STOP_THRESHOLD:Float = 2.0 'min speed before the scrolling stops
	Const SLIP_THRESHOLD:Int = 600 'how far you can drag the map before it stops
	
	Field x:Int
	Field y:Int
	Field w:Int
	Field h:Int
	
	'these define the edge of the map box
	
	Field hasSlipped:Bool
	
	Field x_acc:float
	Field y_acc:float
	
	Field isLocked:bool
	
	Field board:GameBoard
	
	Field lastX:Int
	Field lastY:int
	
	Field camera:Camera
	Method New(x:Int, y:Int, w:Int, h:Int, board:GameBoard)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
		Self.board = board
		camera = board.camera
		camera.SetCameraBounds(x, y, w, h)
		camera.CenterCamera()
		camera.SetMapEdge(board.edgeXMin - Tile.SIZE / 2, board.edgeYMin - Tile.SIZE / 2, board.edgeXMax + Tile.SIZE / 2, board.edgeYMax + Tile.SIZE / 2)
	End
	Method Render:Void()
		DrawRectOutline(x, y, w, h)
		Local temp:Float[] = New Float[4]
		GetScissor(temp)
		SetScaledScissor(x, y, w + 1, h + 1)
		board.Render()
		
		SetScissor(temp[0], temp[1], temp[2], temp[3])
	End
	
	
	Method Update:Void()
		If isLocked Then Return
		If Game.Cursor.Hit() and isMouseOver()
			lastX = Game.Cursor.x()
			lastY = Game.Cursor.y()
			hasSlipped = False
			'	Print "Pos: (" + (Game.Cursor.x() -camera.x) + "," + (Game.Cursor.y() -camera.y) + ")"
			Local xx:Int = (Game.Cursor.x() -camera.x)
			Local yy:Int = (Game.Cursor.y() -camera.y)
			If xx > camera.edgeXMin And xx < camera.edgeXMax And yy > camera.edgeYMin And yy < camera.edgeYMax
				
			Else
					Print "out of bounds"
			End
		End
		If Game.Cursor.Down() And hasSlipped = False and isMouseOver()
			If Abs(lastX - Game.Cursor.x()) > THROW_THRESHOLD
				x_acc = lastX - Game.Cursor.x()
				If x_acc < 0
					x_acc -= DEFAULT_SPEED
				Else
					x_acc += DEFAULT_SPEED
				End
			Else
				x_acc = 0
			End
			
			If Abs(lastY - Game.Cursor.y()) > THROW_THRESHOLD
				y_acc = lastY - Game.Cursor.y()
				If y_acc < 0
					y_acc -= DEFAULT_SPEED
				Else
					y_acc += DEFAULT_SPEED
				End
			Else
				y_acc = 0
			End
			camera.x -= lastX - Game.Cursor.x()
			lastX = Game.Cursor.x()
			camera.y -= lastY - Game.Cursor.y()
			lastY = Game.Cursor.y()
			
			
			'//slip testing
			If camera.x > (camera.edgeXMin * -1) + 256 + x + SLIP_THRESHOLD
				hasSlipped = True
			End
			If camera.y > (camera.edgeYMin * -1) + 256 + y + SLIP_THRESHOLD
				hasSlipped = True
			End
			If camera.x < (camera.edgeXMax * -1) - 256 + x + w - SLIP_THRESHOLD
				hasSlipped = True
			End
			If camera.y < (camera.edgeYMax * -1) - 256 + y + h - SLIP_THRESHOLD
				hasSlipped = True
			End

		Else
			If x_acc <> 0
				camera.x -= x_acc
				If x_acc < 0
					x_acc += FRICTON
				Else
					x_acc -= FRICTON
				End
				If Abs(x_acc) < STOP_THRESHOLD Then x_acc = 0
			End
			If y_acc <> 0
				camera.y -= y_acc
				If y_acc < 0
					y_acc += FRICTON
				Else
					y_acc -= FRICTON
				End
				If Abs(y_acc) < STOP_THRESHOLD Then y_acc = 0
			End
		
			If camera.x > (camera.edgeXMin * -1) + Tile.SIZE + x
				camera.x -= Abs(camera.x - ( (camera.edgeXMin * -1) + Tile.SIZE + x + 8)) / 10.0
				x_acc *= FRICTION_MULT
			End
			If camera.y > (camera.edgeYMin * -1) + Tile.SIZE + y
				camera.y -= Abs(camera.y - ( (camera.edgeYMin * -1) + Tile.SIZE + y + 8)) / 10.0
				y_acc *= FRICTION_MULT
			End
			If camera.x < (camera.edgeXMax * -1) - Tile.SIZE + x + w
				camera.x += Abs(camera.x - ( (camera.edgeXMax * -1) - Tile.SIZE + x + w)) / 10.0
				x_acc *= FRICTION_MULT
			End
			If camera.y < (camera.edgeYMax * -1) - Tile.SIZE + y + h
				camera.y += Abs(camera.y - ( (camera.edgeYMax * -1) - Tile.SIZE + y + h)) / 10.0
				y_acc *= FRICTION_MULT
			End
		
			'	If camera.x < (tempXmax * -512) - 256 + x + w Then camera.x += Abs(camera.x - ( (tempXmax * -512) - 256 + x + w)) / 5.0
		End
		board.Update()
'		
'		For Local temp:Int = 0 Until map.nodes.Length()
'			For Local orbCount:Int = 0 Until map.nodes.Get(temp).orbs.Length()
'				map.nodes.Get(temp).orbs.Get(orbCount).Update()
'			Next
'			If map.nodes.Get(temp).isVisible() And map.nodes.Get(temp).isMouseOver()
'				is_over_node = True
'				If currentNode <> map.nodes.Get(temp).GetName() Then currentNode = map.nodes.Get(temp).GetName()
'				If Game.Cursor.Hit
'					'	Print "name: " + map.nodes.Get(temp).GetName()
'					If map.nodes.Get(temp).isClickable()
'						is_down_node = True
'					End
'					
'				End
'				
'			End
'		Next
'		For Local temp:Int = 0 Until map.doodads.Length()
'			map.doodads.Get(temp).Update()
'		Next
	End
	
	Method CenterCamera:Void(node:string = "")
'		For Local temp:Int = 0 Until map.nodes.Length()
'			If node = map.nodes.Get(temp).GetName()
'				camera.CenterCamera(map.nodes.Get(temp).startX, map.nodes.Get(temp).startY)
'				Return
'			End
'		Next
		camera.CenterCamera()
	End
	Method isMouseOver:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < (x + w) And Game.Cursor.y() > y And Game.Cursor.y() < (y + h)
			Return True
		End
		Return False
	End
	Method isMouseDown:Bool()
		Return Game.Cursor.Down and isMouseOver()
	End
	
End

#rem
Class World
Field x:Int
Field y:Int
Field frameW:Int
Field frameH:Int
Field w:Int
Field h:Int
	
Global water:Image
Global land:Image
	
Method New:(x:Int, y:Int, frameW:Int, frameH:int, w:Int, h:Int)
Self.x = x
Self.y = y
Self.w = w
Self.h = h
Self.frameW = frameW
Self.frameH = frameH
If water = Null
water = LoadImage("water.png")
End
If land = Null
land = LoadImage("land.png")
End
		
End
End
#end



Class Camera
	Field x:Int
	Field y:int
	
	Field boundX:Int
	Field boundY:Int
	Field boundW:Int
	Field boundH:Int
	
	Field edgeXMin:Int
	Field edgeXMax:Int
	Field edgeYMin:Int
	Field edgeYMax:int
	
	Method SetCameraBounds:Void(x:Int, y:Int, w:Int, h:Int)
		boundX = x
		boundY = y
		boundW = w
		boundH = h
	'	SetMapEdge(x, y, x + w, y + h)
	End
	
	Method SetMapEdge:Void(xMin:Int, yMin:Int, xMax:Int, yMax:Int)
		edgeXMin = xMin
		edgeXMax = xMax
		edgeYMin = yMin
		edgeYMax = yMax
	End
	
	Method isInFrame:bool(cx:Int, cy:Int, w:Int, h:Int)
		If cx + (w / 2) < boundX Then Return False
		If cx - (w / 2) > boundX + boundW Then Return False
		If cy + (h / 2) < boundY Then Return False
		If cy - (h / 2) > boundY + boundH Then Return False
		Return True
	End
	
	Method isCursorInFrame:Bool()
		Return isInFrame(Game.Cursor.x, Game.Cursor.y, 0, 0)
	End
	
	Method CenterCamera:Void()
		Self.x = boundX + (boundW / 2)
		Self.y = boundY + (boundH / 2)
	End
	Method CenterCamera:Void(x_pos:Int, y_pos:Int)
		Self.x = boundX + (boundW / 2) - x_pos
		Self.y = boundY + (boundH / 2) - y_pos
	End
End

Class PanelObject
	Field startX:float
	Field startY:float
	Field width:Float
	Field height:Float
	Field camera:Camera
	
	
	Method New(startX:Int, startY:Int, width:Int, height:Int, camera:Camera)
		Self.startX = startX
		Self.startY = startY
		Self.width = width
		Self.height = height
		Self.camera = camera
	End
	
	Method GetX:float()
		Return startX
	End
	
	Method GetY:float()
		Return startY
	End
	
	Method UpdateBounds:Void(x:Int, y:Int, width:Int, height:Int)
		Self.startX = startX
		Self.startY = startY
		Self.width = width
		Self.height = height
	End
	
	Method UpdatePosition:Void(x:float, y:float)
		startX = x
		startY = y
	End
	
	Method Move:Void(dx:float, dy:float)
		UpdatePosition(GetX() +dx, GetY() +dy)
	End
	
	Method Render:Void()
		DrawBoundingBox()
	End
	Method DrawBoundingBox:Void()
		If isMouseOver() = True
			SetColor(255, 0, 0)
		End
		DrawRectOutline(startX - (width / 2) + camera.x, startY - (height / 2) + camera.y, width, height)
		DrawCircle(CenterX(), CenterY(), 5)
		SetColor(255, 255, 255)
	End
	Method CenterX:Int()
		Return startX + camera.x
	End
	Method CenterY:int()
		Return startY + camera.y
	End
	Method GetXMin:Int()
		Return (camera.edgeXMin + (width / 2))
	End
	Method GetXMax:Int()
		Return (camera.edgeXMax - (width / 2))
	End
	Method GetYMin:Int()
		Return (camera.edgeYMin + (height / 2))
	End
	Method GetYMax:Int()
		Return (camera.edgeYMax - (height / 2))
	End
	Method isVisible:Bool()
		Return camera.isInFrame(CenterX(), CenterY(), width, height)
	End
	Method isMouseOver:bool()
		If isVisible() = False Then Return False
		If Game.Cursor.x() > startX - (width / 2) + camera.x And Game.Cursor.x() < startX + (width / 2) + camera.x And Game.Cursor.y() > startY - (height / 2) + camera.y And Game.Cursor.y() < startY + camera.y + (height / 2)
			Return True
		End
		Return False
	End
End