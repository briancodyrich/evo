Strict

Import idle_prototype


Class GameBoard
	Field camera:Camera
	Field edgeXMin:int
	Field edgeXMax:int
	Field edgeYMin:int
	Field edgeYMax:int
	Field tiles:Tile[]
	
	Field paused:Bool
	Field evoSkip:bool
	Field hasRendered:bool
	Field time:int
	Field timeMult:int
	
	Field foodStack:Stack<Food>
	Field life:LifeStack
	
	Method New(w:int, h:int)
		camera = New Camera()
		edgeXMin = 0
		edgeYMax = 0
		edgeXMax = w * Tile.SIZE
		edgeYMax = h * Tile.SIZE
		tiles = New Tile[w * h]
		For Local x:Int = 0 Until w
			For Local y:Int = 0 Until h
				tiles[ (x * w) + y] = New Tile(x * Tile.SIZE, y * Tile.SIZE, camera)
			Next
		Next
		foodStack = New Stack<Food>()
		'For Local temp:Int = 0 Until 15
		'	foodStack.Push(New Food(Rnd(edgeXMin, edgeXMax), Rnd(edgeYMax, edgeYMax), camera))
	'	Next
		life = New LifeStack()
		life.Push(New Life(Rnd(edgeXMin, edgeXMax), Rnd(edgeYMin, edgeYMax), camera))
	End
	
	Method Render:Void()
		hasRendered = True
		For Local temp:Int = 0 Until tiles.Length()
			If tiles[temp].isVisible() Then tiles[temp].Render()
		Next
		For Local temp:Int = 0 Until foodStack.Length()
			If foodStack.Get(temp).isVisible() Then foodStack.Get(temp).Render()
		Next
		For Local temp:Int = 0 Until life.Length()
			If life.Get(temp).isVisible() Then life.Get(temp).Render()
		Next
	End
	
	Method Update:Void()
		If KeyHit(KEY_SPACE)
			paused = not paused
			timeMult = 0
			evoSkip = False
		End
		If KeyHit(KEY_1)
			timeMult = 0
		End If
		If KeyHit(KEY_2)
			timeMult = 1
		End If
		If KeyHit(KEY_3)
			timeMult = 2
		End If
		If KeyHit(KEY_4)
			timeMult = 4
		End If
		If KeyHit(KEY_5)
			timeMult = 8
		End If
		If KeyHit(KEY_6)
			evoSkip = True
		End If
		If evoSkip and hasRendered
			hasRendered = False
			Local ms:Int = Millisecs()
			While life.Length() < 250 and (Millisecs() -ms) < 10000
				'Print(Millisecs() -ms)
				MobUpdate()
			Wend
			If life.Length() >= 250
				evoSkip = False
			End
		Else
			For Local temp:Int = 0 Until (60 * timeMult) + 1
				MobUpdate()
			Next
		End
	End
	Method MobUpdate:Void()
		If paused and not evoSkip Then Return
		time += 1
		While foodStack.Length() < 30 'Clamp(75 - life.Length() * 0.15, 0.0, 75.0)
			foodStack.Push(New Food(Rnd(edgeXMin, edgeXMax), Rnd(edgeYMin, edgeYMax), camera))
		Wend
		For Local l:Life = EachIn life
			Local code:Int
			Local lFood:Int
			Local rFood:Int
			Local lHerb:Int
			Local rHerb:Int
			Local lCarn:Int
			Local rCarn:Int
			Local bCarn:int
			Local foodAtFeet:Int
			Local herbAtFeet:Int
			Local carnAtFeet:Int
			For Local temp:Int = 0 Until foodStack.Length()
				If foodStack.Length() < 80 and Rnd(100000) < 1
					Local posx:Int = -1
					Local posy:Int = -1
					While posx < edgeXMin or posx > edgeXMax or posy < edgeYMin or posy > edgeYMax
						posx = foodStack.Get(temp).x + Rnd(-300, 300)
						posy = foodStack.Get(temp).y + Rnd(-300, 300)
					End
					foodStack.Push(New Food(posx, posy, camera))
				End If
				
				code = l.getSensorReading(foodStack.Get(temp).x, foodStack.Get(temp).y)
				If code = Life.LEFT_SENSOR Then lFood = 1.0
				If code = Life.RIGHT_SENSOR Then rFood = 1.0
				'If code = Life.BACK_SENSOR Then ba = 1.0
				If code <> 0
					If Distance(l.x, l.y, foodStack.Get(temp).x, foodStack.Get(temp).y) < 20
						
						If l.bite and l.isCarn = False
							l.Eat(foodStack.Get(temp).Eat())
						Else
							foodAtFeet = 1.0
						End
					End
					If code = Life.LEFT_SENSOR
						lFood = 1.0
					Else If code = Life.RIGHT_SENSOR
						rFood = 1.0
					End
					If foodStack.Get(temp).IsDead()
						foodStack.Remove(temp)
					End
				End
			Next
			For Local temp:Int = 0 Until life.Length()
				If life.Get(temp) <> l
					code = l.getSensorReading(life.Get(temp).x, life.Get(temp).y)
					If code <> 0
						If Distance(l.x, l.y, life.Get(temp).x, life.Get(temp).y) < 15
							If life.Get(temp).isCarn
								carnAtFeet = 1.0
							Else
								herbAtFeet = 1.0
							End
							If l.isCarn and l.bite
								l.Bite(life.Get(temp))
							End
						End
						If life.Get(temp).isCarn
							If code = Life.LEFT_SENSOR Then lCarn = 1.0
							If code = Life.RIGHT_SENSOR Then rCarn = 1.0
							If code = Life.BACK_SENSOR Then bCarn = 1.0
						Else
							If code = Life.LEFT_SENSOR Then lHerb = 1.0
							If code = Life.RIGHT_SENSOR Then rHerb = 1.0
						End
					End
				End
			Next
			l.Update(lFood, rFood, lHerb, rHerb, lCarn, rCarn, bCarn, foodAtFeet, herbAtFeet, carnAtFeet)
			Local dx:Float
			Local dy:float
			If l.x < edgeXMin
				dx = edgeXMax
			End
			If l.x > edgeXMax
				dx = -edgeXMax
			End
			If l.y < edgeYMin
				dy = edgeYMax
			End
			If l.y > edgeYMax
				dy = -edgeYMax
			End
			l.x += dx
			l.y += dy
			l.Move(dx, dy)
			If l.fitness > 100
				l.fitness -= 50
				life.Push(New Life(l))
			End If
			If l.isDead()
				life.RemoveEach(l)
			End
		Next
		If life.Length() < 100
			life.Push(New Life(Rnd(edgeXMin, edgeXMax), Rnd(edgeYMin, edgeYMax), camera))
		End
		If Rnd(60) < 1
			life.Push(New Life(Rnd(edgeXMin, edgeXMax), Rnd(edgeYMin, edgeYMax), camera))
		End
	End
End